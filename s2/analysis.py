#!/usr/bin/env python3

from math import *
from cmath import polar
from statistics import *
from collections import *
from pprint import pprint
from time import time
import os
# Need Numpy/Scipy
from scipy.ndimage.filters import gaussian_filter
from scipy.signal import argrelextrema
from numpy.fft import rfft, rfftfreq
from numpy import array, square, trapz, absolute, greater, less

# The only data that's worth anything is Voltage & Position

def pull_key(text, key):
    va1 = text.find(key)
    va2 = text.find("&",va1)
    v = [ list(map(float,line.split("\t")))  for line in text[va1:va2].split("\n")[1:-1]]
    return v

def read_set(no):
    text = open("diffraction_lab/set" + str(no) + ".txt", 'r').read()
    
    # Time sampled voltages, at 10 Hz
    timevolts = pull_key(text, '@    legend string 9 "Voltage-A"')
    position = pull_key(text, '@    legend string 7 "Position-1+2"')
    
    return timevolts, position


SCREEN_DIST = 1.000 # +/- 0.001
# Wikipedia indicates that, if this _is_ the cheapest type, max deviation is 10nm
WAVELENGTH = 650e-9
    
# Groups of things representing the same phenomenon
targets = [ ("single-160",{100:(2,3,4,7),10:(5,6),1:(8,9)}),
            ("single-80",{100:(11,12),10:(13,14),1:(15,16)}),
            
            ("double-40-250",{100:17,10:18,1:19}),
            ("double-40-500",{100:20,10:21,1:22}),
            ("double-80-250",{100:23,10:24,1:25}),
            ("double-80-500",{100:26,10:27,1:28}),
            
            ("n-40-125-2",{100:30,10:31,1:32}),
            ("n-40-125-3",{100:33,10:34,1:35}),
            ("n-40-125-4",{100:40,10:37,1:41}),
            ("n-40-125-5",{100:42,10:43,1:44})]

def shift(v, by):
    if by >= len(v):
        raise Exception("Yikes!")
    if by > 0:
        v = v[by:] + by * [0.]
    elif by < 0:
        v =  (-by) * [0.] + v[:by]
    return v

def interpolate_nones(q):
    # Pad ends with zeros; linearly interpolate; cull ends
    # NOTE: Cubic spline interpolation preferred
    q = [0.] + q + [0.]
    i = 1
    while i < len(q) - 1:
        if q[i] == None:
            # Linear interpolation, point by point.
            # Taking this algo because shockingly meh
            j = i
            while q[j] == None:
                j += 1
            q[i] = q[i-1] + (q[j] - q[i-1]) / (j - i + 1)
        i += 1
    q = q[1:-1]
    return q

def lsq_match(basis, gold, given, cap):
    # Technically basis is irrelevant
    errs = trapz(square(array(gold) - array(given)), basis)
    return errs

def get_n_maxima(basis, sequence, n):
    # Return the top N maxima, if possible
    rng = max(sequence) - min(sequence)
    mn = min(sequence)
    
    for thresh in [mn + x*rng/20 for x in reversed(range(21))]:
        # Scan going downward each time until there are five groups
        groups = [[]]
        for b,v in zip(basis,sequence):
            if v > thresh:
                groups[-1].append((b,v))
            elif groups[-1]:
                groups.append([])
        if len(groups) - 1 == n:
            groups = groups[:-1]
            mxs = []
            for group in groups:
                mxs.append(max(group, key=lambda x:x[1]))
            return mxs
    return None

def get_initial_slope(fbasis, ffted):
    """
    Return the slope, in V/d(1/m)
    """
    mx = max(ffted)
    mn = min(ffted)
    halfpoint = (mx + mn) / 2
    # We know it starts near the maximum
    for b,v in zip(fbasis,ffted):
        if v < halfpoint:
            slope = (mx - v) / (b - fbasis[0])
            inte = b + v / slope
            return slope, inte
    return None

def read_data_for_expt(target, opts):
    things = []
    for gain, runs in opts.items():
        if not isinstance(runs, tuple):
            runs = (runs,)
        for run in runs:
            tv, pos = read_set(run)
            if len(tv) != len(pos):
                print("Yikes!")
                quit()
            # Fun part #1: Linearizing!!
            restructured = defaultdict(list)
            mx = max(p[1] for p in pos)
            mn = min(p[1] for p in pos)
            if abs(mn) > abs(mx):
                # Fix inversion in a way that keeps quantization
                pos = [(t,p - mn) for t,p in pos]

            for (_,p),(_,v) in zip(pos,tv):
                # Round to collapse floating point foo
                restructured[round(p,8)].append(v/gain)
            linear = []
            for p,vs in sorted(restructured.items()):
                linear.append((p,mean(vs)))

            things.append((gain, linear))
    things = sorted(things)
    basis = sorted(set(p for q in things for p,_ in q[1]))
    # Must ensure constant step for the basis.
    # It _would_ have been better to calculate stuff based on
    # the raw counts, but oh well.
    
    mstep = 10
    for b1,b2 in zip(basis,basis[1:]):
        step = b2 - b1
        mstep = min(step, mstep)
    top = max(basis)
    bot = min(basis)
    steps = (top - bot) / mstep
    # Force the new basis just to be more regular.
    basis = [bot + j * mstep for j in range(0,int(steps)+1)]

    # Pad everything to have the basis, and then linearly interpolate
    thing2 = []
    for gain, dat in things:
        q = []
        j = 0
        for p in basis:
            # Close enough -- sub-quant level rounding
            if j < len(dat) and abs(p - dat[j][0]) < mstep / 3:
                q.append(dat[j][1])
                j += 1
            else:
                q.append(None)
        # TODO: Figure out cubic interpolation, for extra smoothness!
        q = interpolate_nones(q)
        thing2.append((gain,q))
    things = thing2

    # Before we unify, dump FFTs to file just to see what they're like
    N = 2048
    spacing = mean(a2-a1 for a1,a2 in zip(basis, basis[1:]))
    fbasis = list(rfftfreq(n=N, d=spacing))
    best_fft = []
    if not os.path.exists("log/"+target+".dat"):
        with open("log/"+target+"-fft.dat", "w") as f:
            for i, (gain, dat) in enumerate(things):
                ffted = list(absolute(rfft(dat,n=N)))
                for p,v in zip(fbasis, ffted):
                    if v is not None:
                        print("{:.6g}\t{:.6g}".format(p,v),file=f)
                print("@    s{} color {}".format(i,i+1),file=f)
                print("@    legend string {} \"G={}\"".format(i,gain),file=f)
                print("&",file=f)
                if gain == 1:
                    # All fft results have the same basis
                    best_fft.append(ffted)
    else:
        for i, (gain, dat) in enumerate(things):
            if gain == 1:
                ffted = list(absolute(rfft(dat,n=N)))
                best_fft.append(ffted)
            
    # Since means are linear... but note that we merge magnitudes,
    # ignoring phase shift.
    merged_fft = [mean(x) for x in zip(*best_fft)]

    # #2: Unification

    # We select the first thing as "gold" and XI shift the rest.
    space = [(i,j/1000) for i in range(-10,10) for j in range(-10,10)]
    gold = things[0][1]

    for i,(gain, linear) in enumerate(things):
        if i == 0:
            continue

        # First, X-shift by least squares. Brute force necessary due to instability
        cap = 4.8 / gain # No higher voltages. It appears the thing _was_ +/- 5 V.
        record = 1e9
        for dx in range(-500,500):
            errs = lsq_match(basis, gold, shift(linear, dx), cap)
            if errs < record:
                record = errs
                bdx = dx

        linear = shift(linear, bdx)

        # Next, I-shift by least squares.
        possible_off = [i/10000 for i in range(-20,20)]
        record = 1e20
        for dI in possible_off:
            errs = lsq_match(basis, gold, [x+dI for x in linear], cap - abs(dI))
            # Bias towards center
            errs *= (10+(abs(dI)/max(possible_off)))
            if errs < record:
                record = errs
                bdI = dI
        linear = [x+bdI for x in linear]
        things[i] = (gain,linear)

    # Log intermediates
    if not os.path.exists("log/"+target+".dat"):
        with open("log/"+target+".dat","w") as f:
            for i,thing in enumerate(things):
                for p,v in zip(basis, thing[1]):
                    if v is not None:
                        print("{:.6g}\t{:.6g}".format(p,v),file=f)
                print("@    s{} color {}".format(i,i+1),file=f)
                print("&",file=f)

    # Crude merge strategy. Need something better
    values = [[] for b in basis]
    for gain, linear in things:
        cap = 4.8 / gain
        for i,v in enumerate(linear):
            if v is not None and v < cap:
                values[i].append(v)
    values = [(mean(v) if v else None) for v in values]
    
    # Return basis, values, and the combined (simply averaged) Fourier spectra TODO
    return (basis,values),(fbasis,merged_fft)

def fourier_analyze(fbasis, fvalues, fspacelog, target):
    # It so happens that single slit ≅ sinc. And that FFT's quite nicely!
    # Furthermore, because single-slittiness is high-frequency, you can 
    # pull the slit widths out of everything else!
    
    # For the double-slit results, there is an additional sets of triangular 
    # peaks, corresponding the spacing between the slits.
    
    # For the N-slit results, we get peaks at varying locations. It appears
    # the upslope's are what best determine values. Peaks also have noise near
    # the bottom, due to less-important stuff.
    
    # Note that the maxima are misleading, as only the upslopes appear to match
    
    # We can empirically fit everything by calculating out a class of real spectra,
    # moving them into FFT, and fitting on everything above an (exponential) noise
    # theshold
    
    print("\nTarget:", target, file=fspacelog)

    # Normalization constant due to space transfer, depending on what numpy does
    magic = 1

    # Squared sincs always yield triangles (convolution of hat functions)
    slope, xint = get_initial_slope(fbasis, fvalues)
    first_spatial_freq = 1 / (xint * magic)
    print("X intercept", xint, file=fspacelog)
    print("First spatial freq", first_spatial_freq, file=fspacelog)
    print("Estimated slit width", (SCREEN_DIST * WAVELENGTH) / first_spatial_freq * 1e6, "um", file=fspacelog)

    if target.startswith("single"):
        # Single slit estimate
        pt = get_n_maxima(fbasis, fvalues, 1)
        return
    elif target.startswith("double"):
        target = "n-2"
        ## Double slit
        #pt, pt2 = get_n_maxima(fbasis, fvalues, 2)

        ## Should be slit spacing
        #second_spatial_freq = 1 / (pt2[0] * magic)
        #print("Maxima:",round(pt[0],3),round(pt[1],3))
        #print("Small peak freq:",second_spatial_freq, file=fspacelog)
        #print("Estimated double width:", (SCREEN_DIST * WAVELENGTH) / second_spatial_freq * 1e6, "um", file=fspacelog)
    if target.startswith("n"):
        # n-slot
        deg = int(target.split("-")[-1])
        pts = get_n_maxima(fbasis, fvalues, deg)

        mdiff = mean(p2[0]-p1[0] for p1,p2 in zip(pts,pts[1:])) * magic
        print("Maxima:",*[round(x[0],3) for x in pts], file=fspacelog)
        print("Mean frequency delta:", mdiff, file=fspacelog)
        wavesep = 1 / mdiff
        print("Mean spatial wave separation:", wavesep, file=fspacelog)
        print("Estimated slit spacing:", (SCREEN_DIST * WAVELENGTH) / wavesep * 1e6, "um", file=fspacelog)
    else:
        raise Exception("Unexpected")

def spatial_analyze(basis, values, rspacelog, minima,maxima, target):
    print("\nTarget:", target, file=rspacelog)
    
    center = sum(b*v for b,v in zip(basis,values)) / sum(values)
    print("Mean position:",center,"m", file=rspacelog)
    maxpeak = max(maxima, key=lambda x:x[1])[0] # may be wonky
    print("Max position:", maxpeak,"m", file=rspacelog)
    
    # All of these techniques skip generating+fitting functions
    # because of the diffuse background distortion. If that were fixed...

    if target.startswith("single"):
        # Use point of first separation
        # Pick the first non-peak points
        bef = max(p for p,_ in minima if p < maxpeak - 0.001)
        aft = min(p for p,_ in minima if p > maxpeak + 0.001)
        print("Minima separation:", aft - bef, "m", file=rspacelog)
        width = 2 * SCREEN_DIST * WAVELENGTH / (aft - bef)
        print("Est slit width:", width*1e6 , "um", file=rspacelog)
    elif target.startswith("double"):
        # Repeat analysis on the curve defined by the maxima. Introduces some
        # errors (partly fixable q/ 2nd order interpolation), but if the
        # frequencies match it's close enough. Note that a human would do better.
        # The single-slit frequency can by found by counting the intervening
        # peaks, (but beware fenceposts)

         # Smooth out peaks until we get a much smaller count
        modcurve = gaussian_filter(values, 7)
        submaxima = [(basis[i],values[i]) for i in argrelextrema(array(modcurve), greater)[0]]
        subminima = [(basis[i],values[i]) for i in argrelextrema(array(modcurve), less)[0]]

        bef = max(p for p,_ in subminima if p < maxpeak - 0.001)
        aft = min(p for p,_ in subminima if p > maxpeak + 0.001)
        print("Minima separation:", aft - bef, "m", file=rspacelog)
        width = 2 * SCREEN_DIST * WAVELENGTH / (aft - bef)
        print("Est slit width:", width*1e6 , "um", file=rspacelog)

        # Count the number of minima between bef and aft, and take mean.
        intervmins = sum(1 for v in minima if bef < v[0] < aft)
        subspace = (aft-bef)/intervmins
        print("Mean subwave spacing:", subspace, "m", file=rspacelog)
        width = SCREEN_DIST * WAVELENGTH / subspace
        print("Est slit spacing:", width*1e6 , "um", file=rspacelog)

    elif target.startswith("n"):
        # n-slot
        deg = int(target.split("-")[-1])
        # Reverse engineering N or slot width is harder due to noise/bad sampling

        # We find the spacing of the N greatest maxima, which should be quantized
        j = sorted(maxima, key=lambda x:x[1])[-6:]
        positions = sorted(h[0] for h in j)
        diffs = sorted([x-y for y,x in zip(positions, positions[1:])])
        ideal = mean(diffs[-2:])
        diffs = list(filter(lambda x:x > ideal * 0.7, diffs))
        mdiff = mean(diffs)

        print("High-end peak separation:", mdiff, "m", file=rspacelog)
        width = SCREEN_DIST * WAVELENGTH / mdiff
        print("Est slit spacing:", width*1e6 , "um", file=rspacelog)

        # NOTE: leaving slit widths for later, the trick is enveloping, or,
        # if we subtract the diffuse background, curvature+fitting.
    else:
        raise Exception("Unexpected")

cleanlog = open("log/clean.dat","w")
print("@    legend on", file=cleanlog)

fftlog = open("log/fft.dat","w")
print("@    legend on", file=fftlog)
print("@    xaxis  label \"Cycles/meter\"", file=fftlog)

fspacelog = open("log/analyze-fft.dat","w")
rspacelog = open("log/analyze-real.dat","w")

rsplitextlog = open("log/realsplitextrema.dat","w")
print("@    legend on", file=rsplitextlog)
rjoinextlog = open("log/realallextrema.dat","w")
print("@    legend on", file=rjoinextlog)

for idx,(target, opts) in enumerate(targets):
    # Fun?: Synthesize a signal from all of these. Or, just fit ideal function (+ baseline) w.r.t all of them three times (note the base offset is always different)
    dt = []
    t1 = time()
    (basis, values),(fbasis, fvalues) = read_data_for_expt(target, opts)
    print("\nLoaded",target,"in",time()-t1,"seconds")

    # Record
    print("#",target,file=cleanlog)
    for p,v in zip(basis,values):
        print("{:.6g}\t{:.6g}".format(p,v), file=cleanlog)
    print("@    s{} color {}".format(idx,idx+1), file=cleanlog)
    print("@    legend string {} \"{}\"".format(idx, target), file=cleanlog)
    print("&", file=cleanlog)
    
    maxima = argrelextrema(array(values), greater)[0]
    minima = argrelextrema(array(values), less)[0]

    print("# {} max/min".format(target), file=rsplitextlog)
    for i in maxima:
        print("{:.6g}\t{:.6g}".format(basis[i],values[i]),file=rsplitextlog)
    print("@    s{} color {}".format(idx*2,idx*2+1), file=rsplitextlog)
    print("@    legend string {} \"{}-max\"".format(idx*2, target), file=rsplitextlog)
    print("&",file=rsplitextlog)
    for i in minima:
        print("{:.6g}\t{:.6g}".format(basis[i],values[i]),file=rsplitextlog)
    print("@    s{} color {}".format(idx*2+1,idx*2+2), file=rsplitextlog)
    print("@    legend string {} \"{}-min\"".format(idx*2+1, target), file=rsplitextlog)
    print("&",file=rsplitextlog)
    
    merged = sorted(set(list(minima)+list(maxima)))
    print("# {}".format(target), file=rjoinextlog)
    for i in merged:
        print("{:.6g}\t{:.6g}".format(basis[i],values[i]),file=rjoinextlog)
    print("@    s{} color {}".format(idx,idx+1), file=rjoinextlog)
    print("@    legend string {} \"{}\"".format(idx, target), file=rjoinextlog)
    print("&",file=rjoinextlog)

    print("#",target,file=fftlog)
    for q, v in zip(fbasis, fvalues):
        print("{:.6g}\t{:.6g}".format(q,abs(v)),file=fftlog)
    print("@    s{} color {}".format(idx,idx+1), file=fftlog)
    print("@    legend string {} \"{}\"".format(idx, target), file=fftlog)
    print("&", file=fftlog)

    # Analysis
    qminima = [(basis[i],values[i]) for i in minima]
    qmaxima= [(basis[i],values[i]) for i in maxima]
    fourier_analyze(fbasis, fvalues, fspacelog, target)
    spatial_analyze(basis, values, rspacelog, qminima,qmaxima, target)


