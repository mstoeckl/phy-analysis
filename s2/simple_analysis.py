#!/usr/bin/env python3

#
# Data is copied from the analysis script, and this thing is deliberately orthogonal to it...
#

from math import sqrt

data = {

"single-160":(241.989420347,(0.0,),160,0),

"single-80":(127.33746685,(0.0,),80,0),

"double-40-250":(60.6862009437,(0.0,387.666),40,250),

"double-40-500":(65.1804502903,(0.0,770.877),40,500),

"double-80-250":(127.1184255,(0.0,394.35),80,250),

"double-80-500":(127.916982705,(0.0,775.333),80,500),

"n-40-125-2":(61.8084582734,(0.0,196.061),40,125),

"n-40-125-3":(59.6874959547,(0.0,189.377,376.526),40,125),

"n-40-125-4":(60.1152439537,(0.0,193.833,387.666,581.499),40,125),

"n-40-125-5":(57.3798388179,(0.0,193.833,387.666,581.499,761.965),40,125),
}

# TODO: set found wavelength here, or tune 

def weighted_mean(*x):
    v = sum(y[0]/y[1]**2 for y in x)/sum(1/y[1]**2 for y in x)
    e = sqrt(1/sum(1/y[1]**2 for y in x))
    return v,e 

L = 1.000 # m
err_L = 0.0007 # m
err_freq = 2.0 # 1/m
err_given_a = 5 # um

given_lam = 0.650 # um
measured_lam, err_mlam = weighted_mean((0.6611859302384727,0.021377479793663067),(0.6282518568885761,0.04048900561022652))
print(measured_lam, err_mlam)

for name, (halfwidth, positions,given_a,given_d) in sorted(data.items()):
    N = len(positions)
    if N > 1:
        mspace = (positions[-1]-positions[0])/(N-1)
        err_mspace = 2.0 * sqrt(2) / (N-1)
        
        m2space = [(positions[i]/(i),err_freq/(i)) for i in range(1,N)]
        #print("wmean:",weighted_mean(*m2space),"last:",m2space[-1])

    if name[0] == 's':
        lam = given_a / (halfwidth * L)
        err_lam = lam * sqrt( err_given_a**2 / given_a**2 + err_freq**2/halfwidth**2 + err_L**2/L**2)
        # Result in um (since hw in 1/m, and L in m)
        print(name, given_a, lam, "+/-", err_lam)
        
        print(name, given_a / (lam * L))
    else:.
        # Also calculate _a_ in terms of the rest
        
        exp_d = mspace * measured_lam * L 
        err_exp_d = exp_d * sqrt((err_mspace/mspace)**2+(err_mlam/measured_lam)**2+(err_L/L)**2)
        
        exp_a = halfwidth * measured_lam * L
        err_exp_a = exp_a * sqrt((err_freq/halfwidth)**2+(err_mlam/measured_lam)**2+(err_L/L)**2)

        # Could take N/k for every slit position
        print(name, round(halfwidth, 3), "\t{:.1f} +/- {:.1f}\t{:.1f} +/- {:.1f}\t{:.1f} +/- {:.1f}".format(mspace, err_mspace, exp_a, err_exp_a, exp_d, err_exp_d))
