#!/usr/bin/python

from math import pi, sqrt, atan2, sin, cos, fsum
from statistics import stdev, mean
from scipy.odr import *
import numpy as np

# hand-measured masses 
# spring: 9.9+/-0.1 g, sans twine Can assume it's all in the bulk.
# flag: 12.2+/-0.1 g yep.
# mass A idk which: 0.496 +/- 0.0005 kg (digital scale)
# mass B idk which: 0.495 +/- 0.0005 kg (digital scale)
# which is which, I don't know. Need to calculate expected value/etc. 50/50 odds
# cart: 500 +/- 0.0005 kg exactly.
# weighing procedure; upper and lower
# deltas (can't trust:) B-A = 0.5 to 1.2. C-A is 2.6 to 3. C-B is 4.5 to 4.9.


cart_mass   = 0.500
cart_err    = 0.0005
flag_mass   = 0.0122
flag_err    = 0.0001
spring_mass = 0.0099
spring_err  = 0.0001
two_mass    = 0.991
two_err     = 0.0007
one_mass    = 0.4955
one_err     = 0.0007 # diff is 0.5+/-0.2 g (measured), then err is half + base

def rootsqsum(*s):
    return sqrt(fsum(k**2 for k in s))

def err_sum(*s):
    k = list(s)
    return sum(x[0] for x in k), rootsqsum(*(x[1] for x in k))

masses = {"L":err_sum((cart_mass,cart_err),(flag_mass,flag_err)),
          "M":err_sum((cart_mass,cart_err),(one_mass,one_err),(flag_mass,flag_err)),
          "H":err_sum((cart_mass,cart_err),(two_mass,two_err),(flag_mass,flag_err)),}
print(masses)

shm = [("L",[21,22,24,25,27]),("M",[34,35,36,37,38]),("H",[44,45,46,47,48])]

hand_values = {
    # picked from 2nd and 4th peaks. (which occur in the same location.)
    # next pair from 1st and 2nd peaks (original hand times)
    21:((33.76, 36.20),(32.54,33.76)),
    22:((2.75, 5.22),(1.50,2.75)),
    24:((2.67, 5.10),(1.45,2.68)),
    25:((2.84, 5.30),(1.62,2.84)),
    27:((2.63, 5.09),(1.40,2.62)),

    34:((4.52, 7.94),(2.83,4.52)),
    35:((4.78, 8.16),(3.10,4.77)),
    36:((3.72, 7.19),(2.05,3.73)),
    37:((3.57, 6.95),(1.86,3.57)),
    38:((3.43, 6.87),(1.76,3.43)),
    
    44:((4.50,8.61),(2.47,4.50)),
    45:((4.46,8.60),(2.46,4.47)),
    46:((5.16,9.29),(3.15,5.16)),
    47:((5.36,9.45),(3.34,5.36)),
    48:((4.65,8.78),(2.62,4.62))
    }

length = 1.217
height = 0.1235
gravity = 9.80407 # 154 m elevation, 43.165556 deg north
ruler_error = 0.0005
statics = ("L",0.209,0.212),("M",0.360,0.368),("H",0.501,0.524)
hand_error = 0.02 # s; for central time estimate
gravity_error = 0.001
spacing = 0.010 # meters. i.e. (10 mm)
spacing_error = ruler_error
time_error = 0.00005

def pull_key(text, key):
    va1 = text.find(key)
    va2 = text.find("&",va1)
    v = [ list(map(float,line.split("\t")))  for line in text[va1:va2].split("\n")[1:-1]]
    return v

def read_set_v(no):
    text = open("MSZXDualChannel/set" + str(no) + ".txt", 'r').read()
    v = pull_key(text, "@    legend string 5 \"Speed-1\"")
    s = pull_key(text, "@    legend string 6 \"State-1\"")
    return v,s

def time_filter(data, pre, post):
    return [d for d in data if d[0] > pre and d[0] < post]

def split_by_gap(tseq, scaleup):
    net = []
    sub = tseq[:2]
    spacing = tseq[1][0] - tseq[0][0]
    for (t1,v1),(t2,v2) in zip(tseq[1:], tseq[2:]):
        nsp = t2 - t1
        if nsp > spacing * scaleup:
            net.append(sub)
            sub = []
        spacing = nsp

        sub.append((t2,v2))
    net.append(sub)
    #for row in net:
        #print(len(row))
    return net



def vel_error(raw_vel):
    # sigma_v = (v/s)sqrt(sigma_s^2+2*v^2*sigma_t^2)
    return abs(raw_vel) / spacing * sqrt( spacing_error**2 + 2*(raw_vel*time_error)**2)

def velocity_from_one_pass_state(state):
    seq = []
    ers = []
    for c1,c2 in zip(state,state[2:]):
        t = (c1[0] + c2[0]) / 2
        v = spacing/(c2[0] - c1[0])
        seq.append((t,v))
        ers.append((time_error/sqrt(2),vel_error(v)))
    return seq,ers

def fit_quad(seq_t_y_t_err_y_err):
    t,y,t_err,y_err = np.array(list(seq_t_y_t_err_y_err)).T
     #a(x - h)2 + k,
    model = Model(lambda b,x:b[0]*(x - b[1])**2+b[2])
    # Create a RealData object using our initiated data from above.
    data = RealData(t, y, sx=t_err, sy=y_err)
    # Set up ODR with the model and data. (seed beta array with roughly expected values)
    inbeta = [-1, (t[0]+t[-1])/2, 0]
    odr = ODR(data, model, beta0=inbeta)
    # Run the regression.
    out = odr.run()
    # a, h, k, ea, eh, ek
    return out.beta[0], out.beta[1], out.beta[2], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), sqrt(out.cov_beta[2][2])

def fit_sine(seq_t_y_t_err_y_err):
    t,y,t_err,y_err = np.array(list(seq_t_y_t_err_y_err)).T
    # acos(k(x-h)
    def sine_curve(b,x):
        return b[0]*np.cos(b[2]*(x - b[1]))
    model = Model(sine_curve)
    data = RealData(t, y, sx=t_err, sy=y_err)
    expected_beta = [max(y), (t[0]+t[-1])/2, 1] # a,h,k
    odr = ODR(data, model, beta0=expected_beta)
    out = odr.run()
    # a, _h_, k, ea, _eh_, ek
    return out.beta[0], out.beta[1], out.beta[2], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), sqrt(out.cov_beta[2][2])

def fit_linear(seq_t_y_t_err_y_err):
    t,y,t_err,y_err = np.array(list(seq_t_y_t_err_y_err)).T
    lin_model = Model(lambda b,x:b[0]*x+b[1])
    data = RealData(t, y, sx=t_err, sy=y_err)
    odr = ODR(data, lin_model, beta0=[0., 1.])
    out = odr.run()   
    # slope, intercept, error in slope, error in intercept
    return out.beta[0], out.beta[1], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), out.cov_beta[1][0] 

def weighted_err_mean(sequence_of_vels_and_errors):
    x = list(sequence_of_vels_and_errors)
    bottom = fsum(1/q[1]**2 for q in x)
    wmean = fsum(q[0]/(q[1]**2) for q in x) / bottom
    werr = sqrt(1/bottom)
    return wmean, werr

def pendusolve(sc,mass,err_mass,period,err_period):
    p = sc * pi ** 2
    return (p*mass/period**2, p * sqrt(err_mass**2/period**4+4*err_period**2*mass**2/period**6))

def results(seq_with_errs):
    #print(stdev(x[0] for x in seq_with_errs),mean(x[1] for x in seq_with_errs))
    return (mean(x[0] for x in seq_with_errs),
            stdev(x[0] for x in seq_with_errs)/sqrt(len(seq_with_errs)),
            sqrt(mean(x[1]**2 for x in seq_with_errs)/len(seq_with_errs)),
            *weighted_err_mean(seq_with_errs))

def shm_hand():
    ks = []
    hks = []
    mks = []
    i = 0 
    log_shm = open("log_shm.dat", "w")
    log_plotshm = open("plot_shm.dat", "w")
    log_plotshmc = open("fit_shm.dat", "w")
    log_times = open("log_times.dat", "w")
    log_deltas = open("log_deltas.dat", "w")
    log_plotshmcol = [open("plot_shm_{}.dat".format(i), "w") for i in range(5)]

    allpp = []
    for mn,seq in shm:
        mass,err_mass = masses[mn]
        for iii,s in enumerate(seq):
            i += 1
            vel,state = read_set_v(s)
            if s == 21:
                # outlier, pre/post signal
                vel = time_filter(vel, 31.8, 45)
                state = time_filter(state, 31.8, 45)
            # note: must improve split by gap. It's the inconsistency that matters
            segments = split_by_gap(state, 5)
            i = 0
            centers = []
            log_plot =  open("intermediates/set" + str(s) + ".txt","w")
            for se in segments:
                i += 1
                if i == 7 and s == 47:
                    # outliner, curvy tail
                    continue
                #print(len(se))
                if len(se) in (26,): # proper number of ticks for full passthrough
                    vs,err_vs = velocity_from_one_pass_state(se)
                    # only del on odd runs.
                    #if i % 2 == 1:
                    #    del vs[7:9]
                    for t,v in vs:
                        print("{:.6f}\t{:.6f}".format(t,v), file=log_plot)
                    print("&",file=log_plot)
                    _,h,_,_,eh,_ = fit_sine([(c[0],c[1],k[0],k[1]) for c,k in zip(vs,err_vs)])
                    _,h2,_,_,eh2,_ = fit_quad([(c[0],c[1],k[0],k[1]) for c,k in zip(vs,err_vs)])
                    if abs(h - h2) > 0.01:
                        print("s {: >10.6f} q {: >10.6f} | es {: >10.6f} eq {: >10.6f} | {} {} {}".format(h,h2,eh,eh2,iii,i,s)) 
                    centers.append((h,eh))

            # logging
            fmt = "{:2d}" + "\t{:.3f}\t{:.3f}" * len(centers)
            print(fmt.format(s, *(c for y in centers for c in y) ), file=log_times)
            fmt2 = "{:2d}" + "\t{:.3f}" * (len(centers) - 1)
            print(fmt2.format(s, *(y1[0] - y2[0] for y1,y2 in zip(centers,centers[1:]))), file=log_deltas)
            
            # calculation; only the deltas corresponding to the top half
            qdiffs = [(y[0] - x[0],sqrt(x[1]**2+y[1]**2))for x,y in zip(centers[1::2],centers[2::2])]
            mperiod, err_mperiod = weighted_err_mean(qdiffs)
            mh,err_mh = pendusolve(1,mass,err_mass,mperiod,err_mperiod)
            allpp.append((mass,mh,err_mass,err_mh))

            (t1,t2),(ht1,ht2) = hand_values[s]
            period = t2 - t1
            haperiod = ht2 - ht1
            err_period = hand_error * sqrt(2)
            err_haperiod = hand_error * sqrt(2)

            k, err_k = pendusolve(4,mass, err_mass, period, err_period)
            kh, err_kh = pendusolve(1,mass, err_mass, haperiod, err_haperiod)

            print("{:.3f}\t{:.3f}\t{:.2f}\t{:.2f}".format(2*mperiod, 2*err_mperiod, k, err_kh), file=log_shm)
            print("{:.3f}\t{:.3f}\t{:.3f}\t{:.3f}".format(mass,k,err_mass,err_kh), file=log_plotshm)
            print("{:.3f}\t{:.3f}\t{:.3f}\t{:.3f}".format(mass,k,err_mass,err_kh), file=log_plotshmcol[iii])

            ks.append((k,err_k))
            hks.append((kh,err_kh))
            mks.append((mh,err_mh))
    # linear regression on allp/mass
    s,i,es,ei,csi = fit_linear(allpp)
    print("# s {}+/-{} i {}+/-{}".format(s,es,i,ei), file=log_plotshmc)
    print("0 {}".format(i), file=log_plotshmc)
    print("{} {}".format(2,s*2+i), file=log_plotshmc)

    #print("full %f %f %f %f %f" % results(ks))
    #print("half %f %f %f %f %f" % results(hks))
    print("mach %f %f %f %f %f %f %f" % (*results(mks),i, ei))

def analyze_statics():
    ks = []
    # old: stat 3.715486 0.169229 0.024079 3.675342 0.023697
    # new: stat 3.734568 0.170098 0.006067 3.569697 0.003418
    # ignore errors. also, note cross-correlation in distances -- it gives us better errors
    FxdFdx = []
    for mn, near, far in statics:
        mass, err_mass = masses[mn]
        F = (mass * gravity * height / length)
        m,g,h,l = mass,gravity,height,length
        em,eg,eh,el = err_mass,gravity_error,ruler_error,sqrt(3)*ruler_error
        err_F = rootsqsum(em*g*h/l,eg*m*h/l,eh*m*g/l,el*m*h*g/l/l)
        
        dx = (near + far) / 2
        err_dx = ruler_error / sqrt(2)
        FxdFdx.append((F,dx,err_F,err_dx))
    s,i,err_s,err_i,cov_si = fit_linear(FxdFdx)

    log_stat = open("log_stat.dat","w")
    log_pstat = open("plot_stat.dat","w")
    log_fstat = open("fit_stat.dat","w")
    print("# s {} +/- {} i {} +/- {}".format(s,err_s,i,err_i), file=log_fstat)
    print("0 {}".format(i), file=log_fstat)
    print("{} {}".format(2, i+s*2), file=log_fstat)
    
    for F,x,err_F,err_x in FxdFdx:
        dx = x - i
        err_dx = rootsqsum(err_x, err_i) # (ignoring the covariance, which decreases dx.)
        k = F / dx
        err_k = sqrt(err_F**2 / dx**2 + err_x**2 * F**2/dx**4)
        ks.append((k,err_k))
        
        print("{:.3f}\t{:.3f}\t{:.3f}".format(k, dx, err_k),file=log_stat)
        print("{:.6f}\t{:.6f}\t{:.6f}\t{:.6f}".format(F,x,err_F,err_x),file=log_pstat)
        

    print("stat %f %f %f %f %f" % results(ks))

print("type  mean    stdevofm  rmserrs  wval     werr     extrap   exterr")
shm_hand()
analyze_statics()

# Data table: provide the mean times from each series, as well as value of k derived from it

# Data table: min/max points for the static

"""
    The forces acting on the cart
    How do you obtain k from static measurements and harmonic measurements?
    Where do you put the photogate (you can move it during the experiment)?
    How do you determine the errors in k?
    
    ...
    
    Description of the experiments. Is sufficient detail provided to determine what you actually did? For example: Did you describe the tools that were used in the measurements? Did you include other relevant details of the experiments (e.g. band spacing, mass of the cart, height of the track)?
    Data analysis. We will check for the following details:

    Did you describe how you determined the spring constant for both techniques?
    Did you describe how you determined the errors in the measurements you made?
    Did you describe how you determined the errors in the spring constant for both techniques?
    Did you draw the appropriate conclusions? How do the spring constants obtained with both techniques compare?

    Supporting materials. Quality of data tables. Did you include your data in table format? Use of proper number of significant figures in data tables. Are data tables numbered and referred to in the text? Did you include a table captions that explains what is included in the table? Quality of figures/graphs. Are the x and y axes labelled? Do the axes labels include units? Are there error bars shown when appropriate? Are the figures numbered and referred to in the text? Did you include a figure captions that explains what is shown in the figure?
"""

# Results (quad fit.)
# kshm  = [3.358141, 0.055040]
# kstat = [3.249545, 0.008326]
# knet = [3.251974, 0.008232]
# Results (sine fit.)
# [3.249545, 0.008326] (1.897)
# [3.354549, 0.054071] (-0.292)
# (3.251977, 0.008229)