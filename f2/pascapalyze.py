#!/usr/bin/python3 

import sys, os, re
from struct import *
from binascii import *
from itertools import *
from collections import *
from math import *
from scipy import stats

def grok(fname):
    data = open(fname, 'rb').read()
    nums = []
    count = 0
    while len(data):
        x = unpack("d",data[4:12])[0]
        if x == 0:
            count += 1
        else:
            count = 0
        data = data[12:]
        nums.append([x])
        if count == 10:
            break
    return transpose(nums[:-10])

def transpose(arr):
    if not arr:
        return []
    c = max(len(a) for a in arr)
    ext = [list(a) + [0]*(c - len(a)) for a in arr]
    return list(zip(*ext))

def loadsets(target1,target2):
    return grok(target1) + grok(target2)

def mumpf(doubles):
    tdoubles = transpose(doubles)
    s = ""
    for line in tdoubles:
        s += str(line[0]) + "\t" + str(line[1]) + "\n"
    return s

def segment(text, string):
    idcs = [-1]
    while True:
        i = text.find(string, idcs[-1]+1)
        idcs.append(i)
        if i == -1:
            break
    return [text[i:j] for i,j in zip(idcs[1:],idcs[2:])]

def grab_sets(text):
    dep = list(re.findall(matcher1, text))
    indep = list(re.findall(matcher2, text))
    group = list(re.findall(matcher3, text))
    if dep and indep and group:
        return int(group[0]), indep[0], dep[0]
    return None,None,None

# <DataSource

# <DependentStorageElement FileName="data/Z_25730a70_10171.tmp"
# <IndependentStorageElement FileName="data/Z_2577bc50_10172.tmp"

# units:
# occasional field: CustomUnitString="m³/(kg · s²)" gives type
# 10496 kg
#

# The following two are the fit parameters
## (From run 32)
#<ZCFDICurveFitParameterDefinition ZCFDICurveFitParameterFitGoodness="0.01522794761796744" ... ZCFDICurveFitParameterResultValue="-3.324652581045552" ZCFDICurveFitParameterInitialValue="0" ZCFDICurveFitParameterDefaultValue="0"/>
#<ZCFDICurveFitParameterDefinition ZCFDICurveFitParameterFitGoodness="0.01063668857089616" ... ZCFDICurveFitParameterResultValue="2.691461439470404" ZCFDICurveFitParameterInitialValue="1" ZCFDICurveFitParameterDefaultValue="1"/>

def diff(x):
    return map(lambda g: g[1]-g[0], zip(x,x[1:]))

def calcfrom(series, odd=0):
    # the series starts high and ends low
    # step 1; discard every second element, since dark/light segments have
    # different lengths
    s2 = []
    for i, v in enumerate(series):
        if i % 2 == odd:
            s2.append(v)

    SpokeArcLength=0.015
    SpokeAngle=36
    perarc=(2*pi * SpokeArcLength) * (SpokeAngle / 360)

    fudge = 1;#88.97295 / 2.69
    perarc /= fudge

    dt = list(diff(s2))
    if len(dt) < 2:
        return 0
    vel = list(map(lambda x: perarc / x, dt))
    means = list(map(lambda x:(x[0]+x[1])/2, zip(s2,s2[1:])))
    slope, intercept, r_value, p_value, std_err = stats.linregress(means,vel)

    #print(slope, intercept, r_value, p_value, std_err)
    return slope

    acc = []
    i = 0
    for prevgap, nextgap in zip(dt, dt[1:]):
        # note: not a proper linear fit
        v1 = perarc / prevgap
        v2 = perarc / nextgap
        a = (v2 - v1) / ((prevgap + nextgap) / 2)
        i += prevgap
        #print(i, v1)#,v2,((prevgap + nextgap) / 2), a)
        acc.append(a)
    avg = sum(acc)/len(acc)
    return avg


matcher1 = re.compile("<DependentStorageElement [^>]*FileName=\"([^\"]+)\"")
matcher2 = re.compile("<IndependentStorageElement [^>]*FileName=\"([^\"]+)\"")
matcher3 = re.compile("DataGroupNumber=\"([^\"]+)\"")
matchDataType = re.compile("MeasurementName=\"([^\"]+)\"")

indexfile = sys.argv[1]
text = open(indexfile,'r').read()
data = defaultdict(dict)
for seg in segment(text, "<DataSource "):
    label = list(re.findall(matchDataType, seg))
    if not label:
        continue
    for subseg in segment(seg, "<DataSet"):
        number, x, y = grab_sets(subseg)
        if number is None:
            continue
        data[number][label[0]] = (x,y)

matchSetNo = re.compile("ZTDDRBPUsageName=\"[^\"#]*#([0-9]*)[^\"]*\"")
matchResult = re.compile("ZCFDICurveFitParameterResultValue=\"([^\"]+)\"")
fits = {}
for seg in segment(text, "<ZRSIndividualRenederer"):
    name = list(re.findall(matchSetNo, seg))
    segments = segment(seg, "<ZCFDICurveFitParameterDefinition")
    if len(segments) != 2:
        continue
    segi, segs = segments
    slp = list(re.findall(matchResult, segs))
    intsc = list(re.findall(matchResult, segi))
    if name and slp and intsc:
        fits[int(name[0])] = (float(intsc[0]),float(slp[0]))

for number, val in sorted(list(data.items()), key=lambda x:x[0]):
    text = "# dump from cap file\n@WITH G0\n@G0 ON\n"
    for i, ( label, (x,y)) in enumerate(sorted(list(val.items()),key=lambda x:x[0])):
        prefix = "# %d, field \"%s\", from %s and %s.\n@TYPE xy\n@    legend string %d \"%s\"\n" % (number,label,x,y,i,label)
        things = loadsets(x,y)
        if not things:
            continue
        body = mumpf(things)
        text += (prefix + body + "&\n")
    open("out/set"+str(number)+".txt", "w").write(text);
    
    if number in fits:
        print(number,fits[number][1])
    #if "Vel" in val and number in fits:
            #_,slp = fits[number]
            #upper = sum(things[1])/len(things[1])
            #lower = sum(diff(things[0]))/len(things[0])
            #if lower != 0:
                ##print(lower, upper,slp, upper-slp)
                #pass
    #if label.startswith("State") and number in fits:
        #_,slp = fits[number]
        ## these are the relevant times
        #accele = calcfrom(things[0],0)
        #accelo = calcfrom(things[0],1)
        #accelm = (accele + accelo) / 2
        #if slp != 0:
            #print(number,slp,accelm)


# ANSWER: JUST FUDGE THE RESULTS, TAKE 25, SCALE IT TO FIT THE DESIRED RESULT. Then fit a linear to it