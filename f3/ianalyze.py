#!/usr/bin/python3 

from math import sqrt,fsum
from scipy.stats import linregress
from scipy.odr import *
import numpy as np
from sys import stderr

step_size = 0.01 # m
step_error = 0.0005 # m
time_error = 0.00005 # 10 kHz sample, off by half
force_error = 0.04475104311543812 / 2 # newtons; half the quantization step
force_ts = 0.002 # 500Hz

sets_of_interest = [(0.505,0.005,[25,26,29,30,31]),(1.005,0.005*sqrt(2),[34,35,36,37,38]),(1.505,0.005*sqrt(3),[40,41,42,43,44])]

fkey = "@    legend string 3 \"Force, ChA\""
vkey = "@    legend string 7 \"Velocity, Ch1\""
def read_set_fv(no):
    text = open("ImpulseMSZX/set" + str(no) + ".txt", 'r').read()
    f1 = text.find(fkey)
    f2 = text.find("&",f1)
    v1 = text.find(vkey)
    v2 = text.find("&",v1)
    velocities = [ list(map(float,line.split("\t")))  for line in text[v1:v2].split("\n")[1:-1]]
    forces = [ list(map(float,line.split("\t")))  for line in text[f1:f2].split("\n")[1:-1]]
    return forces, velocities

def tsegment(lod, t1, t2):
    fpre = []
    fmid = []
    fpost = []
    for t,f in lod:
        if t < t1:
            fpre.append((t,f))
        elif t > t2:
            fpost.append((t,f))
        else:
            fmid.append((t,f))
    return fpre, fmid, fpost

def integrate2(fseq):
    # Two point integration
    return fsum((f1+f2)*(t2-t1)/2 for ((t1,f1),(t2,f2)) in zip(fseq, fseq[1:]))
def integrate3(fseq):
    # Three point integration. Thankfully, time steps constantly
    return fsum((t2-t1)/6*(f1+4*f2+f3) for ((t1,f1),(t2,f2),(t3,f3)) in zip(fseq, fseq[1:],fseq[2:]))

def get_region_bounds(f, target):
    # get the points bounding the 90% area
    # we use the fact that points are equally spaced; and there is one maximum
    rec = 0 # max abs
    idx = 0
    for i,(t,v) in enumerate(f):
        if abs(v) > rec:
            rec = abs(v)
            idx = i
    s = 0
    ltl = ltr = f[idx][0]
    for i,((tl,vl),(tr,vr)) in enumerate(zip(f[idx+1:],f[idx-1::-1])):
        s += (tl - ltl) * vl + (ltr - tr) * vr
        ltl = tl
        ltr = tr
        if abs(s) > abs(target):
            break
    return tr,tl
    


def hammer(seq_t_y_t_err_y_err):
    t,y,t_err,y_err = np.array(list(seq_t_y_t_err_y_err)).T
    lin_model = Model(lambda b,x:b[0]*x+b[1])
    # Create a RealData object using our initiated data from above.
    data = RealData(t, y, sx=t_err, sy=y_err)
    # Set up ODR with the model and data.
    odr = ODR(data, lin_model, beta0=[0., 1.])
    # Run the regression.
    out = odr.run()   
    # slope, intercept, error in slope, error in intercept
    return out.beta[0], out.beta[1], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), out.cov_beta[1][0] 

def vel_error(raw_vel):
    # sigma_v = (v/s)sqrt(sigma_s^2+2*v^2*sigma_t^2)
    return abs(raw_vel) / step_size * sqrt( step_error**2 + 2*(raw_vel*time_error)**2)

prefix = "\\begin{tabular}{cccccccc}\n\n\# & $\\unitfrac[|v_{1}|]{m}{s}$ & $\\unitfrac[|v_{2}|]{m}{s}$ & $\\unitfrac[|p_{1}|]{kg\\cdot m}{s}$ & $\\unitfrac[|p_{2}|]{kg\\cdot m}{s}$ & $\\unit[|W_{f}|]{J}$ & $\\unit[|\Delta p|]{N\cdot s}$ & $\\unit[|J_{\\epsilon}|]{N\cdot s}$\\tabularnewline"
print(prefix)
print()
iii = 0
for mass, err_mass,group in sets_of_interest:
    print("\hline")
    print()
    #print("\n mass=",mass)
    #print("\tv1\t\tv2\t\tp1\t\tp2\t\twork\t\tJ-meas\t\tJ-fvel\t\tpercerr")
    for s in group:
        iii += 1
        f,v = read_set_fv(s)
        # we estimate ground via the forces occuring before and after the first and last
        # velocity estimates
        t1,t2 = v[0][0],v[-1][0]
        fpre,fmid,fpost = tsegment(f, t1, t2)
        fbg = fpre + fpost
        foffset = fsum(g[1] for g in fbg)/len(fbg)
        fcormid = [(g[0],g[1]-foffset) for g in fmid]
        impulse = integrate3(fcormid)
        # assuming no errors in the time sampling, or that (more likely)
        # the system auto-corrects for that
        err_impulse = force_ts * force_error * sqrt(len(fcormid))

        tpre,tpost = get_region_bounds(fcormid, impulse * 0.90)

        # len(v) = 25; the first 12 and last 12 are good.
        vfront = v[:12]
        vback = v[-12:]
        # the fifth element in vback always experiences a dip, so we nuke the 
        # datapoint
        del vback[4] # todo decide

        vfs, vfi, err_vfs, err_vfi, fcovar = hammer((q[0],q[1],time_error,vel_error(q[1])) for q in vfront)
        vbs, vbi, err_vbs, err_vbi, bcovar = hammer((q[0],q[1],time_error,vel_error(q[1])) for q in vback)
        # thankfully, we only care about the sum of the two and can skip the 
        # covariance work
        drag_acc = (vfs + vbs) / 2 # slope of v in time
        err_drag_acc = 0.5 * sqrt(err_vfs**2 + err_vbs**2)
        
        v1 = (tpre - vfront[0][0]) * drag_acc + vfront[0][1]
        v2 = (tpost - vback[-1][0]) * drag_acc + vback[-1][1]
        # we assume tpre/tpost have no error
        err_v1 = sqrt(((tpre - vfront[0][0]) * err_drag_acc)**2 + vel_error(vfront[0][1])**2)
        err_v2 = sqrt(((tpre - vback[0][0]) * err_drag_acc)**2 + vel_error(vback[0][1])**2)
        
        Wf = abs(mass * drag_acc * (v1 + v2) * (tpost - tpre) / 2) # negative, since drag_acc is negative
        
        err_v1pv2 = sqrt(err_v1**2+err_v2**2)
        err_tpp = sqrt(2) * time_error
        err_Wf = 1/4 * abs(Wf) * sqrt( (err_mass/mass)**2 + (err_drag_acc/drag_acc)**2 + (err_v1pv2/(v1+v2))**2 + (err_tpp/(tpost-tpre))**2)

        # direct calculation printing
        p1 = mass * v1
        err_p1 = abs(p1) * sqrt((err_mass/mass)**2 + (err_v1/v1)**2)
        p2 = mass * v2
        err_p2 = abs(p2) * sqrt((err_mass/mass)**2 + (err_v2/v2)**2)
        deltaP = p1 + p2
        err_deltaP = sqrt(err_p1**2 + err_p2**2)
        q= (iii,
            v1,err_v1,
            v2,err_v2,
            p1,err_p1,
            p2,err_p2,
            Wf,err_Wf,
            deltaP,err_deltaP,
            abs(impulse),err_impulse)
            #2*(impulse-imest_new)/(impulse+imest_new),0)
        s = "{: .3f}\pm{:.4f}\t"*(len(q)//2)
        #print(s.format(*q))
        sq = "{} & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.3f}$ & ${:.4f}\pm{:.5f}$ & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.4f}$\\tabularnewline"
        print(sq.format(*q))
        print()
        print(impulse, -deltaP, err_impulse, err_deltaP, file=stderr)
        #round(vfe,prec),"\t",round(vbe,prec),"\t",round(vlmid,prec),"\t",round(impulse,prec),"\t",round(imest_plus,prec),"\t",round(2,prec))
print("\\end{tabular}")
print()