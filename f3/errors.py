#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.odr import *
from math import sin

import random

# Initiate some data, giving some randomness using random.random().
#x = np.arange(200)
#y = np.array([4*sin(i) for i in x]) * 1
x = np.array([-1,0,1])
y = np.array([-1,0,1])

sc = 1
x_err = np.array([1 for i in x])*sc
y_err = np.array([1 for i in x])*sc

# Define a function (quadratic in our case) to fit the data with.
def quad_func(p, x):
     m, c = p
     return m*x**2 + c

# Create a model for fitting.
lin_model = Model(lambda b,x:b[0]*x+b[1])

# Create a RealData object using our initiated data from above.
data = RealData(x, y, sx=x_err, sy=y_err)

# Set up ODR with the model and data.
odr = ODR(data, lin_model, beta0=[0., 1.])

# Run the regression.
out = odr.run()

# Use the in-built pprint method to give us results.
out.pprint()
'''Beta: [ 1.01781493  0.48498006]
Beta Std Error: [ 0.00390799  0.03660941]
Beta Covariance: [[ 0.00241322 -0.01420883]
 [-0.01420883  0.21177597]]
Residual Variance: 0.00632861634898189
Inverse Condition #: 0.4195196193536024
Reason(s) for Halting:
  Sum of squares convergence'''

errSlp = np.sqrt(out.cov_beta[0][0]) # sigma_x = sqrt(var_x_x)
errInt = np.sqrt(out.cov_beta[1][1])
print("errSlp:",errSlp,"errInt:",errInt)

x_fit = np.linspace(x[0], x[-1], 1000)
y_fit = quad_func(out.beta, x_fit)

#plt.errorbar(x, y, xerr=x_err, yerr=y_err, linestyle='None', marker='x')
#plt.plot(x_fit, y_fit)

#plt.show()