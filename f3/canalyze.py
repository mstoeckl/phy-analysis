#!/usr/bin/python3 

from math import sqrt,fsum
from scipy.stats import linregress
import numpy as np
from scipy.odr import *
from sys import stderr

# my hand writing gives: 7,8,9,10,13
# the numbers say:       6,7,9,10,13
# (also, the #8 written numbers are BS)

step_size = 0.01 # m
step_error = 0.0005 # m
time_error = 0.00005 # 10 kHz sample, off by half

masses = {'L':(0.505, 0.005), 
          'H':(1.005, 0.005*sqrt(2))}

sets_of_interest = [('L','L',True,[6,7,9,10,13]),
                    ('H','L',True,[21,22,23,24,26]),
                    ('L','H',True,[27,28,29,31,32]),
                    ('L','L',False,[43,44,45,46,47]),
                    ('H','L',False,[48,49,50,51,52]),
                    ('L','H',False,[33,37,38,39,41])]

hand_data = {
        # hand
        #7:[0.32,0,0,0.30],
        #8:[0.32,0,0,0.23],
        # redone (different datasets, 7 is changed)
        
        # IncomingInitial, Outgoing Initial, Incoming Final,   Outgoing Final
        # AI               AO                BI                BO
        6:[0.32,0,0,0.30],
        7:[0.25,0,0,0.23],
        9:[0.31,0,0,0.28],
        10:[0.38,0,0,0.35],
        13:[0.28,0,0,0.23],
        
        21:[0.23,0.05,0,0.28],
        22:[0.23,0.06,0,0.28],
        23:[0.23,0.06,0,0.31],
        24:[0.23,0.05,0,0.28],
        26:[0.21,0.05,0,0.35],
        
        27:[0.40,-0.12,0,0.24],
        28:[0.40,-0.13,0,0.25],
        29:[0.47,-0.13,0,0.29],
        31:[0.42,-0.13,0,0.26],
        32:[0.48,-0.15,0,0.31],
        
        33:[0.47,0.15,0,0.15],
        37:[0.45,0.15,0,0.15],
        38:[0.37,0.12,0,0.12],
        39:[0.52,0.17,0,0.17],
        41:[0.48,0.16,0,0.16],
        
        43:[0.50,0.25,0,0.25],
        44:[0.49,0.25,0,0.25],
        45:[0.51,0.26,0,0.26],
        46:[0.35,0.17,0,0.17],
        47:[0.40,0.19,0,0.19],
        
        48:[0.37,0.25,0,0.25],
        49:[0.34,0.22,0,0.22],
        50:[0.32,0.21,0,0.21],
        51:[0.41,0.28,0,0.28],
        52:[0.47,0.31,0,0.31]
    }

vakey = "@    legend string 9 \"Velocity, Ch1\""
vbkey = "@    legend string 10 \"Velocity, Ch2\""
def read_set_vv(no):
    text = open("CollisionMSZX/set" + str(no) + ".txt", 'r').read()
    va1 = text.find(vakey)
    va2 = text.find("&",va1)
    vb1 = text.find(vbkey)
    vb2 = text.find("&",vb1)
    va = [ list(map(float,line.split("\t")))  for line in text[va1:va2].split("\n")[1:-1]]
    vb = [ list(map(float,line.split("\t")))  for line in text[vb1:vb2].split("\n")[1:-1]]
    return va, vb

def avg(seq):
    q = list(seq)
    return fsum(q) / len(q)

def vel_error(raw_vel):
    # sigma_v = (v/s)sqrt(sigma_s^2+v^2*sigma_t^2)
    err = abs(raw_vel) / step_size * sqrt( step_error**2 + 2*(raw_vel*time_error)**2)
    #        m / s / m                   sqrt (m^2 + (m/s * s)^2
    return err

def weighted_err_mean(sequence_of_vels_and_errors):
    x = list(sequence_of_vels_and_errors)
    bottom = fsum(1/q[1]**2 for q in x)
    wmean = fsum(q[0]/(q[1]**2) for q in x) / bottom
    werr = sqrt(1/bottom)
    return wmean, werr

def hammer(seq_t_y_t_err_y_err):
    t,y,t_err,y_err = np.array(list(seq_t_y_t_err_y_err)).T
    #print(t,y,t_err,y_err)
    lin_model = Model(lambda b,x:b[0]*x+b[1])
    # Create a RealData object using our initiated data from above.
    data = RealData(t, y, sx=t_err, sy=y_err)
    # Set up ODR with the model and data.
    odr = ODR(data, lin_model, beta0=[0., 1.])
    # Run the regression.
    out = odr.run()   
    # slope, intercept, error in slope, error in intercept
    #print("Q",out.beta)
    #print(out.cov_beta)
    return out.beta[0], out.beta[1], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), out.cov_beta[1][0] 


def get_jump_halves(va):
    j = 0
    rec = 0
    # todo?: cubic interpolate the profile and find the midpoint (max jerk) that way
    for i,(v1,v2) in enumerate(zip(va,va[1:])):
        if abs(v2[1] - v1[1]) > rec:
            rec = abs(v2[1] - v1[1])
            j = i + 1
    safety = 2 # skip two ticks before and after the collision
    bef = va[:j-safety]
    aft = va[j+safety:]
    return bef, aft

header1 = r"\begin{tabular}{cccccccc}"
header2 = r"\# & $v_{in}$ ($\unitfrac{m}{s}$) & $v_{1}$ ($\unitfrac{m}{s}$) & $v_{2}$ ($\unitfrac{m}{s}$) & $E_{in}$ ($\unit{\unit{J}}$) & $E_{out}$ ($\unit{\unit{J}}$) & $p_{in}$ ($\unitfrac{kg\cdot m}{s}$) & $p_{out}$($\unitfrac{kg\cdot m}{s}$)\tabularnewline"
print(header1,"\n",header2)
print()

iii = 0
for mass_a_s,mass_b_s,elastic,group in sets_of_interest:
    print(r"\hline ")
    print()
    mass_a, err_mass_a = masses[mass_a_s]
    mass_b, err_mass_b = masses[mass_b_s]
    #print("\n    ",mass_a, "-->",mass_b)
    #print("#n\t "+"\t ".join("vai,hai,vao,hao,vbi,hbi,vbo,hbo,dia,dib,hia,hib,lov%,loh%,cmp".split(",")))
    #print("#n\t"+"\t ".join("vai,vao,vbo,Ein,Eout,pin,pout".split(",")))
    for s in group:
        iii += 1
        va, vb = read_set_vv(s)
        # data from set 8 is wonky.

        if elastic:
            # va traps the collision; vb tracks the collided-into cart
            if mass_a == mass_b:
                vai = va[-1][1]#avg(g[1] for g in va)
                err_vai = vel_error(vai)
                vao = 0
                err_vao = 0 # trivial
            else:
                bef, aft = get_jump_halves(va)
                #vao = avg(g[1] for g in aft)
                #vai = avg(g[1] for g in bef)
                vao, err_vao = weighted_err_mean((g[1],vel_error(g[1])) for g in aft)
                vai, err_vai = weighted_err_mean((g[1],vel_error(g[1])) for g in bef)

            vbo = max(g[1] for g in vb)
            err_vbo = vel_error(vbo) # error in maximum?
            vbi = 0
            err_vbi = 0 # trivial

            if mass_b > mass_a:
                # direction correction
                vao = -vao
        else:
            # vb traps the collision; va records what the result does 
            # (it's backwards)
            bef, aft = get_jump_halves(vb)
            vai, err_vai = weighted_err_mean((g[1],vel_error(g[1])) for g in bef)
            vbi = 0
            err_vbi = 0

            # we could use two estimators for final velocity, but we trust the
            # second channel more
            #e1 = avg(g[1] for g in aft)
            
            # we trust the second channel more
            #slope, intercept, rval,pval,err = linregress(va)
            seq = va
            slope, intercept, err_slope, err_intercept, covariance = hammer((g[0],g[1],time_error,vel_error(g[0])) for g in seq)
            #slope2, intercept2, err_slope2, err_intercept2, covariance2 = hammer((g[0]-5,g[1],time_error,vel_error(g[0])) for g in seq)
            tw = va[0][0]
            e2 = tw * slope + intercept
            # assuming error in tw is not to be mentioned. Otherwise it's a pain
            # statistical intuition
            err_e2 = sqrt((tw*err_slope)**2 + err_intercept**2 + 2*tw*covariance) / sqrt(len(seq) - 2)
            #print(e1,e2)
            vao = vbo = e2
            err_vao = err_vbo = err_e2

        hai,hao,hbi,hbo = hand_data[s]

        dia = mass_a * (vao - vai) # Delta I for A
        dib = mass_b * (vbo - vbi) # Delta I for B
        #print(dia,dib)

        #print(hao,hai,hbo,hbi)
        hia = mass_a * (hao - hai) # Hand Delta I for A
        hib = mass_b * (hbo - hbi) # Hand Delta I for B
        #print(("{:2} "+"\t{: 0.3f}"*14+"\t {}").format(s, vai,hai,vao,hao,vbi,hbi,vbo,hbo,
                                              #dia,dib,hia,hib,(dia+dib)/dia,(hia+hib)/hia,(dia+dib)>(hia+hib) ))
                                              
                                              
        Ein = vai **2 * mass_a / 2
        err_Ein = Ein / 2 * sqrt( 2 * (err_vai/vai)**2 + (err_mass_a/mass_a)**2)
        
        Eout = vao **2 * mass_a / 2+vbo **2 * mass_b / 2
        err2_EoutA = 0.25 * (2 * (mass_a * err_vao)**2 + (vao * err_mass_a)**2)
        err2_EoutB = 0.25 * (2 * (mass_b * err_vbo)**2 + (vbo * err_mass_b)**2)
        err_Eout = sqrt(err2_EoutA + err2_EoutB)
        
        pin = vai * mass_a
        err_pin = pin * sqrt( (err_mass_a / mass_a)**2 + (err_vai / vai)**2)
        
        pout = vao * mass_a + vbo * mass_b
        if vao == 0:
            err_pa2o = 0
        else:
            err_pa2o = (vao * mass_a) * sqrt( (err_mass_a / mass_a)**2 + (err_vao / vao)**2)
        err_pb2o = (vbo * mass_b) * sqrt( (err_mass_b / mass_b)**2 + (err_vbo / vbo)**2)
        err_pout = sqrt(err_pa2o**2 + err_pb2o**2)

        s = (iii,
             vai, err_vai,
             vao, err_vao,
             vbo, err_vbo,
             Ein, err_Ein,
             Eout,err_Eout,
             pin, err_pin,
             pout,err_pout
             )
        #print(("{:2} "+"\t{: 0.3f}\pm{:0.3f}"*(len(s)//2)).format(*s))
        if iii <= 5:
            t = "{}"
        else:
            t = "{:.3f}"
        fmt = r"{} & ${:.3f}\pm{:.3f}$ & $"+t+r"\pm"+t+r"$ & ${:.3f}\pm{:.3f}$ & ${:.4f}\pm{:.4f}$ & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.3f}$ & ${:.3f}\pm{:.3f}$\tabularnewline"
        print(fmt.format(*s))
        print()
        
        print(pin,pout,err_pin,err_pout,file=stderr)
        # WE can trust the computer timing; it shows less loss on average
        # analysis technique: take the maximum velocity before and after the jump
        
print("\\end{tabular}")
print()