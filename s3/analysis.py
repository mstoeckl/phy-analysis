#!/usr/bin/env python3 

"""

All runs with 100x on the bulb side (assuming line noise predominates)

Numbers, rerun with bulb off between runs; (iirc 10x int & 250hz)

2 = 36
3 = 46
4 = 39
5 = 41
6 = 42
7 = 43

With 0.3mm filter widths (at 100x gain, save for 7 at 10x)

5 = 50
4 = 52
3 = 53
2 = 54
6 = 55
7 = 57

With 1.0 filter widths, at 1 kHz

2 = 58
3 = 59
4 = 60
5 = 61
6 = 62
7 = 63
1 = 64
0 = 65/66

7 = 67 With a long warmup time
7 = 68 With 

Translation calibration:
0.017 -> 47.211 over 45.0 deg
Slits are at 62.6
Collimating at 52.6
20 cm away is the next lens

Always using DC, for 8/3 times more power!


"""

import numpy as np
from numpy.fft import rfft, rfftfreq, ifft
from numpy import array, square, trapz, absolute, greater, less
from scipy.signal import savgol_filter, butter, lfilter, filtfilt, medfilt, boxcar, convolve
from math import sqrt, sin, exp, pi
from statistics import mean, stdev
from scipy.misc import derivative
from scipy.optimize import minimize, differential_evolution
from collections import defaultdict
from scipy.odr import ODR, Model, RealData
import random


best = list(reversed([(1,64),(2,58),(3,59),(4,60),(5,61),(6,62),(7,63)]))
#best = [(2,36),(3,46),(4,39),(5,41),(6,42),(7,43)]

# The only data that's worth anything is Voltage & Position

def pull_key(text, key):
    va1 = text.find(key)
    va2 = text.find("&",va1)
    v = [ list(map(float,line.split("\t")))  for line in text[va1:va2].split("\n")[1:-1]]
    return v

def read_set(no):
    text = open("blackbody/set" + str(no) + ".txt", 'r').read()
    
    timevolts = pull_key(text, '@    legend string 13 "Voltage-A"')
    position = pull_key(text, '@    legend string 1 "Angle-1+2"')

    # Note: These latter two have their own distinct & predictable waveforms
    # What comes out: "@    legend string 12 \"Voltage Ch A-B\""
    # What goes in: "@    legend string 9 \"Output Voltage-C\""
    # Current draw: "@    legend string 6 \"Current Probe-B\""
    
    vout = pull_key(text, "@    legend string 12 \"Voltage Ch A-B\"")
    vin = pull_key(text, "@    legend string 9 \"Output Voltage-C\"")
    #cpass = pull_key(text, "@    legend string 6 \"Current Probe-B\"")
    cpass = pull_key(text, "@    legend string 8 \"Output Current-C\"")
    
    return timevolts, position, vout, vin, cpass

def dump_set(name, no, xvals, yvals, xlabel="X",ylabel="Y"):
    with open("log/{}-{}.dat".format(name, no),"w") as f:
        print("@    xaxis  label \"{}\"".format(xlabel), file=f)
        print("@    yaxis  label \"{}\"".format(ylabel), file=f)
        for b,row in zip(xvals, yvals):
            if b is None or row is None:
                continue
            print("{}\t{:.6f}".format(b,row), file=f)

def pos_to_wavelen(pos, offset=None):
    ratio = .953511#0.9569
    scale = 1.02
    #init_angle = 76.0# 68.9 <- 76 is way too high...
    if offset == None:
        init_angle = 73.0
    else:
        init_angle = offset

    true_angle = abs(pos)*ratio
    corr_true_angle = init_angle - true_angle
    n = sqrt((2*sin(corr_true_angle*0.017453292519943295)/sqrt(3) + 0.5)**2 + 3/4)
    # n = index of refraction
    
    iz = 1 / (n - 1.635)
    lam = 320 + iz + 0.2*iz**2+ 0.19*iz**3
    
    #print(pos,true_angle, corr_true_angle, n, iz, lam)
    
    # Full light sensor range... clipped to 380 by range covered :-(
    lower, upper = 380, 10000
    #lower, upper = 400, 1000
    
    
    if lam <= lower or lam >= upper:
        return None
    return lam

def blackbody(lam, T, I):
    # T in Kelvin
    # I is total emission
    # lam is in nm
    hc = 1240 # eV-nm
    c = 3e17 # nm/s
    kb = 8.617e-5 # ev/K
    
    B = 8 * pi * I * hc * c / (lam**5)  / ( exp(hc / (lam * kb * T)) - 1)
    return B

def temp_for_peak(lam, dlam):
    # lam in nm; output in K
    return (2897773 / lam, dlam * 2897773 / lam**2)

def max_index_if_defined(noneseq, valseq):
    idx = max([(m,i) for m,w,i in zip(valseq, noneseq, range(len(valseq))) if w is not None], key=lambda z:z[0])[1]
    return idx

def width_at_height(pos, vals, target):
    f = min(z for z,v in zip(pos, vals) if z and v > target)
    g = max(z for z,v in zip(pos, vals) if z and v > target)
    return g -f


klog = open("log/res.dat", "w")
print("@TYPE XYDXDY", file=klog)
clog = open("log/cres.dat", "w")
print("@TYPE XYDY", file=clog)

sumsqa = defaultdict(float)
def proc(V,M):
    tv, tp, vout, vin, cpass = read_set(M)
    print("V",V,M,len(tv),len(tp))
    
    # Determine spacing, just in case
    spacing = (tv[-1][0]-tv[0][0])/(len(tv)-1)
    basis = [spacing * i for i in range(len(tv))]
    
    nyq = 1 / (2 * spacing)

    # Fadout at 20 Hz
    B, A = butter(5, 20 /nyq, btype='low')
    clean_volt = filtfilt(B, A, [v[1] for v in tv])

    # Noise introduction early (resistance test)
    #tv = [(t,v+random.gauss(0, 0.007)) for t,v in tv]

    noise = stdev( (r - s) for r,s in zip(clean_volt[:300], [v[1] for v in tv]))
    orignoise = stdev([v[1] for v in tv[:300]])
    smonoise = stdev(clean_volt[:300])
    print("Noisiness (V): {:.4f} {:.4f} {:.4f} {:.4f} {:.4f}".format(noise, orignoise, sqrt(orignoise**2 - noise**2), smonoise, max(clean_volt)))

    # Add noise & clean again, just to see var
    #clean_volt = [c + random.gauss(0, smonoise) for c in clean_volt]
    #clean_volt = filtfilt(B, A, clean_volt)
    
    # Smooth position to interpolate intermediate locations
    rng = int(0.02*1/spacing) # 0.02 s smoothing; not strictly realistic, oh well
    
    smooth_pos = convolve([tp[0][1]]*rng+[t[1] for t in tp]+[tp[-1][1]]*rng, boxcar(rng) / rng, mode="same")[rng:-rng]


    # Log basic data
    dump_set("orig", V, basis, [t[1] for t in tv], "Timer", "Voltage")
    dump_set("btr", V, basis, clean_volt, "Time", "Voltage")
    dump_set("pos", V, basis, [t[1] for t in tp], "Time", "Position")
    dump_set("spos", V, basis, smooth_pos, "Time", "Position")

    # Correct for background drift, using spatial view.
    imean = mean([c for v,c in zip(smooth_pos, clean_volt) if v > 1 and v < 2])
    max_sp = max(smooth_pos)
    min_sp = min(smooth_pos)
    emean = mean([c for v,c in zip(smooth_pos, clean_volt) if v > max_sp-2 and v < max_sp-1])
    subtr = [imean + (emean - imean) / (max_sp - min_sp) * z for z in smooth_pos]
    mc_volt = [m-s for m,s in zip(clean_volt, subtr)]
    #mc_volt = clean_volt
    
    # Log intermediate data
    dump_set("zpo", V, smooth_pos, clean_volt, "Angle", "Voltage")
    dump_set("adj", V, smooth_pos, mc_volt, "Angle", "Voltage")
    
    ftemp = None
    eftemp = None
    for offset in [72.1]:#[x/10 for x in range(710,750)]:
        wavelen = [pos_to_wavelen(p, offset=offset) for p in smooth_pos]
        # Can find peak via histogram, max voltage, etc...
        peak = max_index_if_defined(wavelen, mc_volt)
        # Eyeballed, includes intermediate microskips
        angular_uncertainty = 0.15
        lam_peak = wavelen[peak]
        
        dlamdang = derivative(pos_to_wavelen, smooth_pos[peak], dx=1e-4)
        elam= dlamdang*angular_uncertainty
        
        epeak = width_at_height(wavelen, mc_volt, mc_volt[peak] - smonoise / 2)
        temp, etemp = temp_for_peak(lam_peak, elam)

        predicted = [blackbody(l, temp, 1.0) if l else None for l in wavelen]
        scale = mc_volt[peak] / max([p for p in predicted if p]) 
        predicted = [p * scale if p else None for p in predicted]
        #meanc = mean(m * (k2 - k1) for m,k1,k2 in zip(mc_volt, wavelen, wavelen[1:]) if k1 and k2)
        #print(meanc)
        ssv = sum(b**2*rat for b,rat in zip(mc_volt, wavelen) if rat)
        #print(ssv)
        error = sum((a-b)**2 for a,b,rat in zip(predicted, mc_volt, wavelen) if a) / ssv
        
        sumsqa[offset] += error
        
        if offset == 72.1:
            ftemp = temp
            eftemp = etemp
            dump_set("wave", V, wavelen, mc_volt, "Wavelength", "Voltage")
            dump_set("bb", V, wavelen, predicted, "Wavelength", "Voltage")
            print(offset, "{:.3f}+/-{:.3f}".format(temp, etemp), "{:.3f}+/-{:.3f}".format(lam_peak, elam), "{:.6e} {:.6e}".format(error, dlamdang))
        
            modvolt = [v for w,v in zip(wavelen,mc_volt) if w is not None]
            
    temp = ftemp
    etemp = eftemp

    # Volt/current guess
    vdiff = abs(mean([v[1] for v in vout]) - mean([v[1] for v in vin]))
    vimn = mean([v[1] for v in vin])
    evin = max(stdev([v[1] for v in vin]),1e-6)
    mcc = abs(mean([c[1] for c in cpass]))
      
    tempc = 300 + ((vimn/mcc)/0.84 - 1) / 0.0090 #/.0045
    print("Wire resistance (ohm) {:.3f} wire temp {:.3f} found temp {:.3f} voltage {:.3f}+/-{}".format(vimn/mcc, tempc, temp, vimn, evin))

    #print(V, temp_for_peak(lam, elam)[0], temp_for_peak(lam, elam)[1], file=klog)
    print("{:.6f}\t{:.6f}\t{:.6f}\t{:.6f}".format(vimn, temp, evin, etemp), file=klog)
    print(V, tempc, tempc/10, file=clog)
        
    return (vimn, temp, evin, etemp)

def linfit(x,y,dx,dy):
    model = Model(lambda b,x:b[0]+b[1]*x)
    data = RealData(x, y, sx=dx, sy=dy)
    odr = ODR(data, model, beta0=[mean(x), 0])
    out = odr.run()
    # i, s, ei, es
    return out.beta[0], out.beta[1], sqrt(out.cov_beta[0][0]), sqrt(out.cov_beta[1][1]), out.res_var

k = []
for V, M in best:
    k.append(proc(V, M))


i,s,ei,es,chired = linfit([e[0] for e in k],[e[1] for e in k],[e[2] for e in k],[e[3] for e in k])
print("T = {:.3f}+/-{:.3f} + {:.3f}+/-{:.3f} * V, X2_red = {:3f}".format(i,s,ei,es,chired))

print("&", file=klog)
print("@TYPE NXY", file=klog)
for v,_,_,_ in k:
    print("{:.6f}\t{:.6f}".format(v, s*v+i), file=klog)

# Need to apply ODR on 

#print(sumsqa.items(), min([(j,k) for j,k in sumsqa.items()], key=lambda x:x[1]))

