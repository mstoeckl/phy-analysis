#!/usr/bin/env python3

# Game plan:
# Step 1, read data, check fft for 60hz/400hz, smooth voltages in time domain, boxcar position
# Step 2, reconstruct/scale angle/voltage graph and show how crude (central peak median)
#         estimates overlap. Do a mirror graph; and see what sort of peak background correction works
# Step 3: Find all the peaks on each shot, and use the collection of them to construct a center line
#         (be sure to use height relative to background as another grouping signal; just
#         do min x^2 on all potential order matchings)
#         Use that center line to construct a collection of line location/height estimates
# Step 4: Actually use the position estimates to calculate wavelengths. Yay!

import sys,os,re
import numpy as np
import scipy.signal as sg
import scipy.interpolate
from collections import defaultdict

def pull_key(text, key):
    va1 = text.find(key)
    va2 = text.find("&",va1)
    v = [ list(map(float,line.split("\t")))  for line in text[va1:va2].split("\n")[1:-1]]
    return v

def read_set(mode,no):
    text = open("tube"+mode+"/set" + str(no) + ".txt", 'r').read()
    
    timevolts = pull_key(text, '@    legend string 11 "Voltage-A"')
    position = pull_key(text, '@    legend string 1 "Angle-1+2"')
    return timevolts, position

def boxsmooth(v,rng):
    v = list(v)
    return list(sg.convolve([v[0]]*rng+v+[v[-1]]*rng, sg.boxcar(rng) / rng, mode="same")[rng:-rng])

def gausssmooth(v,rng):
    v = list(v)
    return list(sg.convolve([v[0]]*rng+v+[v[-1]]*rng, sg.gaussian(rng,int(rng/3)) / rng, mode="same")[rng:-rng])

sets = {
    'A':[9,8,10,11,12],
    'B':[2,3,4,5,6],
    'C':[3,11,13,16,18],
    'N':[1,2,3,4,5]
}

def record(name, k, setid, x_values, y_values):
    with open("log/"+name+"-{}-{}.dat".format(k,setid),"w") as out:
        for x,v in zip(x_values,y_values):
            print("{:.6f}\t{:.6f}".format(x,v),file=out)

os.makedirs("log",exist_ok=True)

cleaned_halves = defaultdict(list)

for k,vs in sorted(sets.items()):
    for setid,i in enumerate(vs):
        tv,tp = read_set(k,i)
        # next, construct common basis
        t_vals = [x[0] for x in tv]
        spacing = (t_vals[-1]-t_vals[0])/(len(t_vals)-1)
        del t_vals # just in case

        v_vals = [x[1] for x in tv]
        p_vals = [x[1] for x in tp]
        
        # clip off the end spikes (within 3 pos. spacings)
        p_step = min(abs(v1-v0) for v1,v0 in zip(p_vals,p_vals[1:]) if v1 != v0)
        for iii,p in enumerate(p_vals):
            if abs(p - p_vals[0]) > 4 * p_step:
                break
        p_vals = p_vals[iii:]
        v_vals = v_vals[iii:]
        for iii,p in enumerate(reversed(p_vals)):
            if abs(p - p_vals[-1]) > 4 * p_step:
                break
        p_vals = p_vals[:-iii]
        v_vals = v_vals[:-iii]

        # Smooth positions to get higher detail
        p_vals = boxsmooth(p_vals, 3)

        # Guess what! 60Hz +/- 1 noise
        # Admittedly, this one _does_ kill a bit too much of it
        if k != 'N': # sodium reference not noisy
            lowcut = 59
            highcut = 61
            b,a = sg.butter(5, [2*lowcut * spacing, 2*highcut * spacing], btype='bandstop')
            v_vals = sg.filtfilt(b,a,v_vals)
        
        # now, apply extra smoothing
        v_vals = boxsmooth(v_vals, 3)

        # merge duplicate positions
        p2,v2 = [p_vals[0]],[[v_vals[0]]]
        for p,v in zip(p_vals[1:],v_vals[1:]):
            if p == p2[-1]:
                v2[-1].append(v)
            else:
                p2.append(p)
                v2[-1] = np.mean(v2[-1])
                v2.append([v])
        v2[-1] = np.mean(v2[-1])
        p_vals = p2
        v_vals = v2

        # reconstruct on a cleaner basis. Fancier interps would need to avoid duplicate positions
        vf = scipy.interpolate.interp1d(p_vals, v_vals, kind='linear')
        step = min(abs(v1-v0) for v1,v0 in zip(p_vals,p_vals[1:]) if v1 != v0)
        p_vals = np.linspace(max(p_vals), min(p_vals), (max(p_vals)-min(p_vals)) / step)
        v_vals = vf(p_vals)
        # Hm: perchance use RBF & smooth meanwhile
        
        # Background subtraction of voltage
        v_bg = gausssmooth(v_vals, 2000)
        v_vals -= v_bg
        
        # Clip out everything under zero....
        v_vals = [max(0,v) for v in v_vals]# -0.002 to nuke low ripples

        # crude centering
        peak_idx = np.argmax(v_vals[int(len(v_vals)*0.3):int(len(v_vals)*0.7)])+int(len(v_vals)*0.3)
        p_vals -= p_vals[peak_idx]

        # fft the voltages, after the fact
        #N = 4096
        #fftv = list(np.absolute(np.fft.rfft(v_vals,n=N)))
        #fftb = list(np.fft.rfftfreq(n=N, d=spacing))
        
        # Center finding:
        # a) We divide into two halves and realign one
        # b) We clip off the inner 10 deg of each half
        # c) We find the offset of maximum autocorrelation (wait -- isn't LSQ offset better?)
        # d) Figure out what that offset means (# of rotations) and convert to a shift)

        # make values move in positive direction
        p_vals = p_vals[::-1]
        v_vals = v_vals[::-1]
        
        # Note: p_vals[(lidx+ridx)//2] = 0.0
        lidx = np.argmax(p_vals > -10)
        ridx = np.argmax(p_vals > 10)
        
        lp = -p_vals[:lidx][::-1]
        lv = v_vals[:lidx][::-1]
        rp = p_vals[ridx:]
        rv = v_vals[ridx:]
        
        # TODO: log lp, lv
        
        corr = sg.correlate(lv,rv,mode='full')# valid same full
        shift = np.argmax(corr) - len(rv)
        offset = shift * (step / 2) # shifting midline doubles relative shift
        
        p_vals += offset
        
        mirror = np.abs(p_vals)
        
        #record("corr", k, setid, range(len(corr)), corr)
        #record("L",k,setid,lp,lv)
        #record("R",k,setid,rp,rv)
        
        record("M",k,setid,mirror,v_vals)
        
        
        
        
        # TODO: need better extrema metrics... (i.e, depth of neighboring troughs is better than amplitude) -- actually, that doesn't solve the low-value clumping with ER-N;
        extrema = list(sg.argrelextrema(np.array(v_vals),np.greater,order=20)[0])
        
        left_extrema = [e for e in extrema if p_vals[e] < -12.0]# furthest in valid point at 13
        right_extrema = [e for e in extrema if p_vals[e] > 12.0]
        left_extrema = sorted(left_extrema, key=lambda i:-v_vals[i])
        right_extrema = sorted(right_extrema, key=lambda i:-v_vals[i])
        counts = {'A':2,'B':7,'C':13,'N':2}
        nelem = counts[k]#+2
        #if k == 'N':
            ## no extras for Na as it has really clean data. Also, it doesn't always show all of its peaks... :-(
            ## hmm... how about a voting/matching algorithm that holds across all peaks
            ## (difficult to avoid O( (n^2)^9 ) though; but taking intersection of top 12's ought to work)
            #nelem = counts[k] + 1
            #perms = [np.array([j]) for j in range(nelem)]
        #else:
            #nelem = counts[k]+2
        perms = [np.array([i,j]) for i in range(nelem) for j in range(nelem) if j > i]
        left_extrema = np.array(sorted(left_extrema[:nelem],key=lambda i:p_vals[i]))
        right_extrema = np.array(sorted(right_extrema[:nelem],key=lambda i:p_vals[i]))

        
        # Need to find a nearest-neighbor matching.... so,
        # step 1: Pick K+2 peaks where K is the number we want/expect
        # step 2: iterate over (45)^2 skipped positions 
        # step 3: minimize the sum of squared distances between matched pairs
        # step 4: done! (be sure to plot in one unit)

        best = (left_extrema[::-1],right_extrema)
        #best = None
        #recval = 1e20
        #for p1 in perms:
            #for p2 in perms:
                #v1 = np.delete(left_extrema, p1)[::-1]
                #v2 = np.delete(right_extrema, p2)
                #l1 = np.abs(p_vals[v1])
                #l2 = np.abs(p_vals[v2])
                
                ## meh. the problem of ripples.... TODO FIXME ABOVE
                #cost = np.sum(np.square(l1 - l2))
                #if cost < recval:
                    #recval = cost
                    #best = (v1,v2)
                
        # TODO ALIGNMENT
        print()
        print("L",p_vals[best[0]])
        print("R",p_vals[best[1]])
        #print("rec",recval)

        # Do in a subfunction...
        #print(left_extrema, right_extrema)

        record("EL", k,setid, [-p_vals[i] for i in best[0]], [v_vals[i] for i in best[0]])
        record("ER", k,setid, [p_vals[i] for i in best[1]], [v_vals[i] for i in best[1]])



        # problem of automatically matching extrema .... (or, just do it by hand? x 50)

        # z) We need to figure out how to find K largest peaks....

        #print("range:", max(p_vals)-min(p_vals))
        ## Technically, ought to ignore all inside +/- 30 deg
        ## grab more than enough peaks...
        #extrema = list(sg.argrelextrema(np.array(v_vals),np.greater,order=10)[0])
        #extrema = [e for e in extrema if abs(p_vals[e]) >= 10.0]
        #left_extrema = [e for e in extrema if p_vals[e] < 0]
        #right_extrema = [e for e in extrema if p_vals[e] > 0]
        #left_extrema = sorted(left_extrema, key=lambda j: -v_vals[j])
        #right_extrema = sorted(right_extrema, key=lambda j: -v_vals[j])
        
        ## clean out everything without a matching peak....
        ## TODO: Figure out a good alignment algorithm... (perhaps w/ spatial transform, autocorrelation, etc)
        
        ##l2 = []
        ##r2 = []
        ##for l in left_extrema:
            ##min_dist = 1000
            ##for r in right_extrema:
                ##pl = abs(p_vals[l])
                ##pr = abs(p_vals[r])
                ##vl = v_vals[l]
                ##vr = v_vals[r]
                ##min_dist = min(min_dist, 20*abs(pl - pr) + abs(vl - vr))
            ##if min_dist < 3:
                ##l2.append(l)
        ##for r in right_extrema:
            ##min_dist = 1000
            ##for l in left_extrema:
                ##pl = abs(p_vals[l])
                ##pr = abs(p_vals[r])
                ##vl = v_vals[l]
                ##vr = v_vals[r]
                ##min_dist = min(min_dist, 20*abs(pl - pr) + abs(vl - vr))
            ##if min_dist < 3:
                ##r2.append(r)

        ## Arg, matching isn't fun...
        #left_peak = p_vals[left_extrema[0]]
        #right_peak = p_vals[right_extrema[0]]
        #print(left_peak, right_peak)
        #offset = (right_peak + left_peak) / 2
        ##p_vals = [p - offset for p in p_vals]

        ## index of central offset
        #cidx = [i for (i,p) in enumerate(p_vals) if p < 0][0]
        #print(cidx)

        #lp = list(reversed(p_vals[:cidx]))
        #rp = [-p for p in p_vals[cidx:]]
        #lv = list(reversed(v_vals[:cidx]))
        #rv = v_vals[cidx:]
        
        #lpi = [i for (i,p) in enumerate(lp) if p > 10][0]
        #rpi = [i for (i,p) in enumerate(rp) if p > 10][0]

        #print(lpi,rpi)
        #cleaned_halves[k].append( (lp[lpi:],lv[lpi:]) )
        #cleaned_halves[k].append( (rp[rpi:],rv[rpi:]) )


            #for v,p in zip(fftv,fftb):
                #print(p,v,file=out)
        
        #with open("log/{}-{}a.dat".format(k,setid),"w") as out:
            #for j in left_extrema:
                #print(p_vals[j],v_vals[j],file=out)
            #for j in right_extrema:
                #print(p_vals[j],v_vals[j],file=out)
        

        # hm: can find steepest local maxima by curvature at a radius (i.e., drop within +/- 0.5 deg)
        
quit()
        
def periodic_corr(x, y):
    return np.fft.ifft(np.fft.fft(x) * np.fft.fft(y).conj()).real
        
for k, v in cleaned_halves.items():
    
    # shortest one...
    v_init = sorted(v,key=lambda x:len(x[1]))[0][1]
    for setid, (p_vals,v_vals) in enumerate(v):
        corr = sg.correlate(v_vals,v_init,mode='same')
        #corr = periodic_corr(v_init,v_vals)
        peak = np.argmax(corr)
        
        # NOTE: probably best to run correlate pairwise for each pair from a single 
        # sample to determine the actual midline estimate
        
        print(len(corr),peak)# corresponds to some sort of shift; what, idk...
        v_vals = np.roll(v_vals, -peak)

        with open("log/C{}-{}.dat".format(k,setid),"w") as out:
            for p,v in zip(p_vals,corr):
                print(p,v,file=out)
        with open("log/V{}-{}.dat".format(k,setid),"w") as out:
            for p,v in zip(p_vals,v_vals):
                print(p,v,file=out)
    # Problem: need to establish uniform spacing, by merging double values
    # (even after smoothing) and linearly interpolating the rest
    #(p1,v1),(p2,v2) = v[0],v[1]
    

    
                
        
        


