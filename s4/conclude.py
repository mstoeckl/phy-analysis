#!/usr/bin/python3

import os
from statistics import mean, stdev
from math import sin,cos,pi,asin,sqrt
from scipy.interpolate import interp1d
from collections import defaultdict

types = {'A':'H','B':'He','C':'Hg','N':'Na'}

def grok(f):
    return [[float(k) for k in j.split()] for j in open(f,"r").readlines()]

def read_data(tp,nk=True):
    ds = []
    vs = []
    for i in range(5):
        d1 = grok("log/EL-"+tp+"-"+str(i)+".dat")
        d2 = grok("log/ER-"+tp+"-"+str(i)+".dat")
        if nk:
            net = grok("log/M-"+tp+"-"+str(i)+".dat")
            vs.append(net)
        ds.append(d1)
        ds.append(d2)
    return ds,vs

def weighted_mean(vlist, elist):
    bottom = sum(1/e**2 for e in elist)
    wmean = sum(v/(e**2) for v,e in zip(vlist,elist)) / bottom
    werr = sqrt(1/bottom)
    return wmean, werr

def save_combo(g,elem,bunches,verts,verts2,rng=(float('-inf'),float('+inf'))):
    out = open("plot/{}.dat".format(g),"w")
    count = 0
    lo,hi = rng
    colors = [3,4,8,9,10,11,12,13,14,15] # gray/black/white reserved; yellow skipped
    print("@    title \"Tube {}\"".format(g),file=out)
    print("@    subtitle \"contains {}\"".format(elem),file=out)
    print("@    xaxis  tick op top",file=out)
    print("@    yaxis  label \"Measured Voltage (V)\"",file=out)
    print("@    xaxis  label \"Angle (deg)\"",file=out)
    for d in bunches:
        d = [(p,v) for (p,v) in d if lo <= p <= hi]
        if not d: 
            continue
        print("@    s{} linestyle 0".format(count),file=out)
        print("@    s{} symbol fill 1".format(count),file=out)
        print("@    s{} color {}".format(count,colors[count % len(colors)]),file=out)
        print("@    s{} symbol 2".format(count),file=out)
        for p,v in d:
            print("{:.6f}\t{:.6f}".format(p,v),file=out)
        print("&",file=out)
        count += 1
    top = 0
    for i in range(5):
        net = grok("log/M-"+g+"-"+str(i)+".dat")
        print("@    s{} color 7".format(count),file=out)
        net = [(p,v) for (p,v) in net if lo <= p <= hi]
        
        top = max(top, max(v for p,v in net))
        if True:
            for p,v in net:
                print("{:.6g}\t{:.6g}".format(p,v),file=out)
            print("&",file=out)
            count += 1
    mto = max(x[1] for x in verts)
    for v,inte in verts:
        print("@    s{} linestyle 2".format(count),file=out)
        print("@    s{} linewidth 2".format(count),file=out)
        print("@    s{} color 1".format(count),file=out)
        print("{:.6g}\t{:.6g}".format(v,0.0),file=out)
        print("{:.6g}\t{:.6g}".format(v,top*inte/mto),file=out)
        print("&",file=out)
        count += 1
    mto2 = max(x[1] for x in verts2)
    verts2 = [(v,i) for (v,i) in verts2 if lo <= v <= hi]
    for v,inte in verts2:
        print("@    s{} linestyle 4".format(count),file=out)
        print("@    s{} linewidth 2".format(count),file=out)
        print("@    s{} color 6".format(count),file=out)
        print("{:.6g}\t{:.6g}".format(v,0.0),file=out)
        print("{:.6g}\t{:.6g}".format(v,top*inte/mto2),file=out)
        print("&",file=out)
        count += 1
    print("@    world xmin {}".format(round(lo,-1)),file=out)
    print("@    world xmax {}".format(round(hi,-1)),file=out)
    print("@    world ymax {}".format(round(top,2)+0.01),file=out)
    s = float("{:.0e}".format(top / 6))
    print("@    xaxis  tick major 5",file=out)
    print("@    xaxis  tick minor 2.5",file=out)
    print("@    yaxis  tick major {}".format(s),file=out)
    print("@    yaxis  tick minor {}".format(s),file=out)



# Light sensor sensitivity curve
l = [283.81287,293.21326,301.90677,311.29095,310.4624,338.08243,377.38565,
    426.30017,492.07724,565.2395,630.35834,727.68726,814.05194,894.6196,
    948.09174,973.09094,993.7556,1004.9023,1015.3339,1027.4794,1034.7091,
    1044.9043,1060.1368,1087.8975,1102.7325]
i = [0.001481567,0.02350304,0.041576084,0.06585202,0.078812115,0.12345597,
    0.1794219,0.22810122,0.2831544,0.3346518,0.37945417,0.4441199,
    0.49972114,0.54346156,0.5718693,0.571975,0.5613533,0.54505503,
    0.5259356,0.4730053,0.3879271,0.2994796,0.22627154,0.14917101,0.13119742]
sensitivity = interp1d(l,i,'cubic')
    

os.makedirs("plot",exist_ok=True)

# Strongest Na lines:
# nm           intens
# 588.9950954  80000
# 589.5924237  40000

Na_ref = 589.19420483

# Second strongest...
# 363.1272   1200
# 371.1070   850

for g,elem in sorted(types.items()):
    ds, vs = read_data(g,False)
    if False:
        # Plot positions & all else
        out = open("plot/{}.dat".format(g),"w")
        count = 0
        for d in ds:
            print("@    s{} linestyle 0".format(count),file=out)
            print("@    s{} symbol fill 1".format(count),file=out)
            print("@    s{} symbol 2".format(count),file=out)
            for p,v in d:
                print("{:.6f}\t{:.6f}".format(p,v),file=out)
            print("&",file=out)
            count += 1
        for w in vs:
            for p,v in w:
                print("{:.6g}\t{:.6g}".format(p,v),file=out)
            print("&",file=out)
            count += 1
    print(" ",g)
    # NOTE: temporarily, we just consider the first two
    
    # We have two constraints: #1, group ordering inside points.
    #print(ds)
    
    olen = max(len(d) for d in ds)
    ns = [d + [(1000,0)]*3 for d in ds] # 3 is buffer
    bunches = []
    #perp = list(zip(*ns))
    while all(len(n) >= 2 for n in ns):
        gen1 = [n[0] for n in ns]
        gen2 = [n[1] for n in ns]

        m1 = min(v[0] for v in gen1)
        m2 = min(v[0] for v in gen2)

        #m1 = mean(v[0] for v in gen1)
        #m2 = mean(v[0] for v in gen2)
        
        rs = []
        bl = []
        for n in ns:
            # NOTE: Really must pair beforehand due to peak matching....
            # ^ or exists there another way?
            if (n[0][0]-m1)**2 <= (n[0][0]-m2)**2 and abs(n[0][0]-m1) < 1.5:
                # closer to this bunch than the next
                if n[0][0] < 999:
                    bl.append(n[0])
                rs.append(n[1:])
            else:
                rs.append(n)
        ns = rs
        if bl:
            bunches.append(bl)

    #ds = ds[:2]
    
    # Well, actually, it might be better to just detect the top K points by salience
    # in conclude, and then do a 10-ary match, with voting, misses over here. Then yield points in confidence order
    #points = [([p1,p2],[v1,v2]) for (p1,v1),(p2,v2) in zip(*ds)]
    
    #print(points)
    
    #spacing = 0.001 / 1200 # 80,100,300, or 600 lines per mm
    spacing = 1660.528 # +/- 0.627 # nm
    espacing = 1.024 # nm
    #spacing = 1666.7 # nm for 600 lines/mm
    #max_wavelen = spacing / 2 # ? wait, it should be spacing
    max_wavelen = 800
    dangle = 0.01

    # d Sin[θ] = m λ
    # -> λ = d * Sin[θ]
    # -> d = λ / Sin[θ]
    # θ = Asin[ m λ / d ] 

    for i,b in enumerate(bunches):
        if len(b) <= 3:
            # note enough votes :-(
            continue
        
        plist = [x[0] for x in b]
        vlist = [x[1] for x in b]
        # Note: convert to wavelengths, then collect stdev (tis easier)
        
        # Guess what spacing results for a given line....
        slist = [Na_ref / sin(angle*pi/180) for angle in plist]
        ms = mean(slist)
        es = stdev(slist) / sqrt(len(slist))
        
        llist = [spacing * sin(angle*pi/180) for angle in plist]
        # Weighted mean proves inappropriate as peak finding/centering hard to analyze
        #ellist = [sqrt( (espacing * sin(angle*pi/180))**2 + (dangle*angle*pi/180 * cos(angle*pi/180))**2) for angle in plist]
        #ml,el = weighted_mean(llist, ellist)
        
        # cm; 

        ml = mean(llist)
        el = stdev(llist) / sqrt(len(llist))
        
        
        mv = mean(vlist)
        ev = stdev(vlist) / sqrt(len(vlist))
        # Although we _do_ get high precision, smoothed peaks of +/- 1 deg are bad
        print(i+1,"   {:6.3f}+/-{:6.3f} nm {:6.3f}+/-{:6.3f} V".format(ml,el,mv,ev))
        if elem == 'Na':
            print("      {:6.3f}+/-{:6.3f} slit nm".format(ms,es))
        if elem == 'H':
            energy = 1240 / ml
            R = 13.605692
            guess = sorted([(i,j) for i in range(1,6) for j in range(1,6)], key=lambda x:abs(R * (1 / x[1]**2 - 1/x[0]**2) - energy))[0]
            restimate = R * (1 / guess[1]**2 - 1/guess[0]**2)
            print("   {:.5f} | {} {:.5f}".format(energy,guess, restimate))

    # Generate Plots
    
    # Grab important lines from NIST data.
    x = open("lines/{}.txt".format(elem),"r").read().split("\n")
    v = defaultdict(int)
    for line in x:
        if not line or line[0] == '-':
            continue
        idx = line.index("|",line.index("|", line.index("|", line.index("|", line.index("|")+1)+1)+1)+1)
        j = [j.strip("* h") for j in line[:idx].split("|")]
        if elem == 'H':
            j = ['H I'] + j
        try:
            mag = float(j[3])
            aki = float(j[4])
            lam = float(j[1])
        except ValueError:
            continue
        if lam >= 300 and lam <= max_wavelen and 'I' in j[0] and 'II' not in j[0]:
            lam = round(lam,1)
            #print(mag,aki)
            if elem == 'Hg':
                # Note: for 'Hg' wannabe, line sequence doesn't seem to match either way
                v[lam] = max(mag * sensitivity(lam),v[lam])#aki
            else:
                v[lam] = max(mag * sensitivity(lam),v[lam])
    v = sorted(v.items(),key=lambda x:-x[1])[:15]
    v = sorted(v,key=lambda x:x[0])
    print(" ", elem)
    for (lam,inte) in v:
        print("{:.1f} {:.2e}".format(lam,inte))
    
    min_angle = asin(300 / spacing)*180/pi
    max_angle = asin(max_wavelen / spacing)*180/pi

    lam = [(asin(lam / spacing)*180/pi,inte) for (lam,inte) in v]
    lam2 = [(asin(2 * lam / spacing)*180/pi,inte) for (lam,inte) in v]
    save_combo(g, elem, bunches, lam, lam2, (min_angle, max_angle))


    
