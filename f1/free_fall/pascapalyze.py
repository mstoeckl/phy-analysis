#!/usr/bin/python3 

import sys
from struct import *
from binascii import *
from itertools import groupby

target1 = "test1.cap"
target2 = "test2.cap"

def a1(data):
    c = b"data/Z"
    runs = data.split(c)
    firsttype = []
    secondtype = []
    for run in runs:
        if run[-8:-4] == b"\x00\x00\xa4\x81":
            secondtype.append(run)
        else:
            firsttype.append(run)
        print(len(run),hexlify(run[-8:]))
    
def get_tag(run):
    a = run.rfind(b'.tmp')
    b = run.find(b'data')
    return run[b:a+4].decode()

def get_type(run):
    return run[:8]

def get_tail(run):
    return run[8:]

def linediff(lor):
    bs = [False]*max(len(r) for r in lor)
    for r in lor:
        for i,c in enumerate(r):
            if bs[i] == c:
                pass
            elif bs[i] is False:
                bs[i] = c
            else:
                bs[i] = True
    s = ""
    for i, val in enumerate(bs):
        if val is False:
            s += ".. "
        elif val is True:
            s += "?? "
        else:
            q = hex(val)[2:]
            k = str(i)
            s += "0"*(2-len(q))+q+" ("+" "*(2-len(k))+k+")"+" "
    return s
    

as01 = [False]*100
id2r = {}

def sub(a,b):
    c = []
    for i,(a,b) in enumerate(zip(a,b)):
        c.append((a-b+256)%256)
    return bytes(c)

previous = None
def proc0102(tail):
    #global previous
    #if previous != None:
        #print( hexlify(sub(previous, tail)))
    #previous = tail
    #print(hexlify(tail))
    
    #print("0102:", hexlify(tail))
    for i, b in enumerate(tail):
        q = as01[i]
        if q == b:
            pass
        elif q is False:
            as01[i] = b
        else:
            as01[i] = True
            
    if len(tail) < 60:
        return
    
    sectionA = tail[10:12]
    sectionB = tail[14:20]
    sectionC = tail[54:58]
    combo = [sectionA, sectionB, sectionC]
    #print(list(map(hexlify,combo)))
    #print(tail[54],tail[55],tail[56],tail[57]) # (values cluster each at 50 and 100)
    #print(tail[14],tail[15],tail[16],tail[17],tail[18])
    #print(unpack("f",tail[10:10+4]))
    #print(unpack("i",tail[10:10+4]))
    
    print(hexlify(sectionB), get_tag(tail))
    
    ## sampleID: two consecutive fields have the same one
    #sampleID = struct.unpack('H',tail[10:12])
    #if sampleID in id2r:
        #if id2r[sampleID] != None:
            #print(linediff([tail, id2r[sampleID]]))
            #pass
        #id2r[sampleID] = None
    #else:
        #id2r[sampleID] = tail
        

    
    ##wblock = tail[12:20]
    ##d = struct.unpack('L', wblock)
    #print(struct.unpack('d', tail[12:20]),struct.unpack('if', tail[12:20]),struct.unpack('Hi', tail[12:20]))
    ##print(wblock)
    #nblock = tail[54:58]
    #if nblock:
        #print(struct.unpack('i', nblock), struct.unpack('f', nblock), struct.unpack('HH', nblock))
    
    #if i[0] != 1:
        #print(i)
    #print(i[0])

as02 = [False]*499
def proc0304(tail):
    #print("0304:", hexlify(tail))
    for i, b in enumerate(tail):
        q = as02[i]
        if q == b:
            pass
        elif q is False:
            as02[i] = b
        else:
            as02[i] = True

def processrun(run):
    head = hexlify(get_type(run))
    tail = get_tail(run)
    if head == b'01022e0314000008':
        proc0102(run)
    elif head == b'0304140000080800':
        proc0304(run)
    else:
        # unidentified, no common, we don't care
        pass

def a2(data):
    runs = data.split(b"PK")
    for run in runs:
        if len(run) < 500:
            #print(len(run), binascii.hexlify(run[:40]))
            #print(binascii.hexlify(get_type(run)), len(run))
            processrun(run)
    s = ""
    for val in as01:
        if val == False:
            s += ".. "
        elif val == True:
            s += "?? "
        else:
            q = hex(val)[2:]
            s += "0"*(2-len(q))+q+" "
    print(s)
    s = ""
    for val in as02:
        if val == False:
            s += " ."
        elif val == True:
            s += "??"
        else:
            q = hex(val)[2:]
            s += "0"*(2-len(q))+q
    #print(s)

def get_runs_for_prefix(runs, prefix):
    for run in runs:
         if hexlify(get_type(run)) == prefix:
             yield get_tail(run)


if __name__ == "__main__":
    data1 = open(target1,"rb").read()
    runs = [run for run in data1.split(b"PK") if len(run) < 500 and len(run) > 60]
    x = []
    for run in get_runs_for_prefix(runs, b'0304140000080800'):
        name = get_tag(run)
        key = unpack('H',run[0:2])
        x.append((name, key, run))
    x = sorted(x, key=lambda x:(x[1],int(x[0].split(".")[0].split("_")[-1])))
    sk = [(k,list(g)) for k,g in groupby(x, lambda x: x[1])]
    for k,g in sorted(sk, key=lambda x:x[0]):
        its = list(g)
        print()
        for (name, key, run) in its:
            # 00 00 80 3f is floating point for 1.0
            front,end = run.split(bytes(name,"utf8"))
            print(name, hexlify(front),hexlify(end))
    
    #data2 = open(target2,"rb").read()
    #a2(data1)
    
    # The data consists of multiple segments, 
    # bunch of zeros, followed by fixed, changing, fixed, then text tag, then fixed, changing fixed
    
    # The transition for the last segment starts with "main.xml" and then we have N repeats
    
    
    # example opening tag: PK | 01 02 2e 03 14 00 00 08 08 00 |
    
    
    # 2E 47 9E 92  A7 6A 1C 00  00 00 E0 2E  00 00 17 00  00 00 00 00  00 00 00 00  00 00 A4 81  DC 74 01 00  64 61 74 61  2F 5A 5F 31  36 64 61 35  63 66 30 5F  33 34 33 2E  74 6D 70 50  4B 01 02 2E  03 14 00 00  08 08 00 37  75 2E
    # | .G..§j....à...............¤.Üt..data/Z_16da5cf0_343.tmpPK..........7u.

    
    
    