#!/usr/bin/env python3


"""

Basically, we record coordinates of line endpoints; from those we get angles;
and the angles (quoted as +/- 1 deg) are all we need.


"""

# ?? what points are needed

from math import *

refls = [
    (43,44),
    (44,44),
    (43,44),
    (44,44),
    (44,44),
    (44,45),
    #(40,35),
    #(40,34),
    #(40,34),
    (70,71),
    (70,71),
    (70,71),
    (45,45),
    (45,45),
    (45,46)]

err = 0.5

diffs = [(x[0]-x[1],sqrt(2)*0.5) for x in refls]
print(diffs)

def weighted_err_mean(sequence_of_vels_and_errors):
    x = list(sequence_of_vels_and_errors)
    bottom = fsum(1/q[1]**2 for q in x)
    wmean = fsum(q[0]/(q[1]**2) for q in x) / bottom
    werr = sqrt(1/bottom)
    return wmean, werr

print(weighted_err_mean(diffs))

convex = [(15.2,0.1),(15.1,0.1)]
print(weighted_err_mean(convex))

rrf = [ ([(40,32),(40,32),(40,32)], 47),
        ([(70,4),(70,4),(70,5)], 44),
        ([(45,24),(45,24),(45,24)], 45) ]
for rq, z in rrf:
    err = 0.5 / sqrt(3)
    mx = sum(j[0] for j in rq) / 3
    my = sum(j[1] for j in rq) / 3
    print(round(err, 1), round(mx, 1), round(my, 1), round(z, 1))

solved = [(1.46436, 0.0119834),(1.43235, 0.0102481),(1.47177, 0.0115851)]
print(weighted_err_mean(solved))